<?php if (!defined('THINK_PATH')) exit();?>﻿<!-- 头部文件 -->
<!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<title>表单</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="format-detection" content="telephone=no">

		<link rel="stylesheet" href="/ygcrafts/public/plugins/layui_new/css/layui.css" media="all" />
		<link rel="stylesheet" href="/ygcrafts/public/plugins/font-awesome/css/font-awesome.min.css">
		<!--  -->
		<link rel="stylesheet" href="/ygcrafts/public/css/comment.css" media="all">
		<link rel="stylesheet" href="/ygcrafts/public/css/table.css" />
		<link rel="stylesheet" href="/ygcrafts/public/css/main.css" />


		<script type="text/javascript" src="/ygcrafts/public/js/jquery-3.1.1.js"></script>
		<script type="text/javascript" src="/ygcrafts/public/plugins/layui_new/layui.js"></script>
		<!-- 图表 -->
		<script type="text/javascript" src="/ygcrafts/public/Highcharts/5.0.6/js/highcharts.js"></script>
		<script type="text/javascript" src="/ygcrafts/public/Highcharts/5.0.6/js/modules/data.js"></script>
		<!-- 公共js -->
		<script type="text/javascript" src="/ygcrafts/public/js/common.js"></script>
	</head>

	<body>


		<!-- 第一层 -->
		<div class="row___3eawN">

			<div class="content___BnbuS">
				<div class="pageHeaderContent___1WBMY">
					<div class="avatar___3yyKI">
						<span class="ant-avatar ant-avatar-lg ant-avatar-circle ant-avatar-image"><img src="https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png"></span>
					</div>

					<div class="content___3h7ql">
						<div class="contentTitle___4Mmy4">
							早安，曾俊豪，祝你开心每一天1111！
						</div>
						<div class="wen_txt">
							交互专家 | 蚂蚁金服－某某某事业群－某某平台部－某某技术部－UED
						</div>
					</div>
				</div>
			</div>

			<div class="extraContent___3mck4">
				<div class="extraContent___1Im9T">
					<div class="statItem___axqF9">
						<p>
							项目数
						</p>
						<p>
							56
						</p>
					</div>
					<div class="statItem___axqF9">
						<p>
							团队内排名
						</p>
						<p>
							8<span> / 24</span>
						</p>
					</div>
					<div class="statItem___axqF9">
						<p>
							项目访问
						</p>
						<p>
							2,223
						</p>
					</div>
				</div>
			</div>
		</div>
		<!-- 第二层 -->
		<div class="content___13wW7">
			<div class="ant-row" style="margin-left: -12px; margin-right: -12px;">
				<!-- 左 -->
				<div style="padding-left: 12px; padding-right: 12px;" class="ant-col-xs-24 ant-col-sm-24 ant-col-md-24 ant-col-lg-24 ant-col-xl-16 wen_index_left">
					<div style="margin-bottom: 24px;" class="ant-card projectList___3oQ1f ant-card-wider-padding ant-card-padding-transition ant-card-contain-grid">

						<div class="ant-card-head">
							<div class="ant-card-head-wrapper">
								<div class="ant-card-head-title">
									进行中的项目
								</div>
								<div class="ant-card-extra">
									<a href="#/">全部项目</a>
								</div>
							</div>
						</div>

						<div class="ant-card-body wen_xxx" >
							  
							<!-- 表格 -->
							<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
							
							<table id="datatable" style="display:none">
							    <thead>
							        <tr>
							            <th></th>
							            <th>注册用户</th>
							            <th>充值记录</th>
							        </tr>
							    </thead>
							    <tbody>
							        <tr>
							            <th>苹果</th>
							            <td>3</td>
							            <td>4</td>
							        </tr>
							        <tr>
							            <th>苹果</th>
							            <td>3</td>
							            <td>4</td>
							        </tr>
							        <tr>
							            <th>梨</th>
							            <td>2</td>
							            <td>7</td>
							        </tr>
							        <tr>
							            <th>葡萄</th>
							            <td>5</td>
							            <td>1</td>
							        </tr>
							        <tr>
							            <th>苹果</th>
							            <td>3</td>
							            <td>4</td>
							        </tr>
							        <tr>
							            <th>梨</th>
							            <td>2</td>
							            <td>7</td>
							        </tr>
							        <tr>
							            <th>葡萄</th>
							            <td>5</td>
							            <td>1</td>
							        </tr>
							
							    </tbody>
							</table>


						</div>
					</div>
						


					<div class="ant-card activeCard___1sEm8 ant-card-wider-padding ant-card-padding-transition">
						<div class="ant-card-head">
							<div class="ant-card-head-wrapper">
								<div class="ant-card-head-title">
									动态
								</div>
							</div>
						</div>
						
						<div class="ant-card-body" style="padding: 0px;">
							<div>
								<div class="ant-list ant-list-lg ant-list-split">
									<div>
										<div class="ant-spin">
											<span class="ant-spin-dot"><i></i><i></i><i></i><i></i></span>
										</div>
									</div>
									<div class="activitiesList___1iu8_">
										<div class="ant-list-item">
											<div class="ant-list-item-meta">
												<div class="ant-list-item-meta-avatar">
													<span class="ant-avatar ant-avatar-circle ant-avatar-image"><img src="https://gw.alipayobjects.com/zos/rmsportal/BiazfanxmamNRoxxVxka.png"></span>
												</div>
												<div class="ant-list-item-meta-content">
													<h4 class="ant-list-item-meta-title"><span><a class="username___3QGFo">曲丽丽</a>&nbsp;<span class="event___17TiG">在 <a href="http://github.com/">高逼格设计天团</a> 新建项目 <a href="http://github.com/">六月迭代</a></span></span></h4>
													<div class="ant-list-item-meta-description">
														<span class="datetime___35Apf" title="Fri Mar 09 2018 16:14:14 GMT+0800">几秒前</span>
													</div>
												</div>
											</div>
										</div>
										<div class="ant-list-item">
											<div class="ant-list-item-meta">
												<div class="ant-list-item-meta-avatar">
													<span class="ant-avatar ant-avatar-circle ant-avatar-image"><img src="https://gw.alipayobjects.com/zos/rmsportal/cnrhVkzwxjPwAaCfPbdc.png"></span>
												</div>
												<div class="ant-list-item-meta-content">
													<h4 class="ant-list-item-meta-title"><span><a class="username___3QGFo">付小小</a>&nbsp;<span class="event___17TiG">在 <a href="http://github.com/">高逼格设计天团</a> 新建项目 <a href="http://github.com/">六月迭代</a></span></span></h4>
													<div class="ant-list-item-meta-description">
														<span class="datetime___35Apf" title="Fri Mar 09 2018 16:14:14 GMT+0800">几秒前</span>
													</div>
												</div>
											</div>
										</div>
										<div class="ant-list-item">
											<div class="ant-list-item-meta">
												<div class="ant-list-item-meta-avatar">
													<span class="ant-avatar ant-avatar-circle ant-avatar-image"><img src="https://gw.alipayobjects.com/zos/rmsportal/gaOngJwsRYRaVAuXXcmB.png"></span>
												</div>
												<div class="ant-list-item-meta-content">
													<h4 class="ant-list-item-meta-title"><span><a class="username___3QGFo">林东东</a>&nbsp;<span class="event___17TiG">在 <a href="http://github.com/">中二少女团</a> 新建项目 <a href="http://github.com/">六月迭代</a></span></span></h4>
													<div class="ant-list-item-meta-description">
														<span class="datetime___35Apf" title="Fri Mar 09 2018 16:14:14 GMT+0800">几秒前</span>
													</div>
												</div>
											</div>
										</div>
										<div class="ant-list-item">
											<div class="ant-list-item-meta">
												<div class="ant-list-item-meta-avatar">
													<span class="ant-avatar ant-avatar-circle ant-avatar-image"><img src="https://gw.alipayobjects.com/zos/rmsportal/WhxKECPNujWoWEFNdnJE.png"></span>
												</div>
												<div class="ant-list-item-meta-content">
													<h4 class="ant-list-item-meta-title"><span><a class="username___3QGFo">周星星</a>&nbsp;<span class="event___17TiG">将 <a href="http://github.com/">5 月日常迭代</a> 更新至已发布状态</span></span></h4>
													<div class="ant-list-item-meta-description">
														<span class="datetime___35Apf" title="Fri Mar 09 2018 16:14:14 GMT+0800">几秒前</span>
													</div>
												</div>
											</div>
										</div>
										<div class="ant-list-item">
											<div class="ant-list-item-meta">
												<div class="ant-list-item-meta-avatar">
													<span class="ant-avatar ant-avatar-circle ant-avatar-image"><img src="https://gw.alipayobjects.com/zos/rmsportal/ubnKSIfAJTxIgXOKlciN.png"></span>
												</div>
												<div class="ant-list-item-meta-content">
													<h4 class="ant-list-item-meta-title"><span><a class="username___3QGFo">朱偏右</a>&nbsp;<span class="event___17TiG">在 <a href="http://github.com/">工程效能</a> 发布了 <a href="http://github.com/">留言</a></span></span></h4>
													<div class="ant-list-item-meta-description">
														<span class="datetime___35Apf" title="Fri Mar 09 2018 16:14:14 GMT+0800">几秒前</span>
													</div>
												</div>
											</div>
										</div>
										<div class="ant-list-item">
											<div class="ant-list-item-meta">
												<div class="ant-list-item-meta-avatar">
													<span class="ant-avatar ant-avatar-circle ant-avatar-image"><img src="https://gw.alipayobjects.com/zos/rmsportal/jZUIxmJycoymBprLOUbT.png"></span>
												</div>
												<div class="ant-list-item-meta-content">
													<h4 class="ant-list-item-meta-title"><span><a class="username___3QGFo">乐哥</a>&nbsp;<span class="event___17TiG">在 <a href="http://github.com/">程序员日常</a> 新建项目 <a href="http://github.com/">品牌迭代</a></span></span></h4>
													<div class="ant-list-item-meta-description">
														<span class="datetime___35Apf" title="Fri Mar 09 2018 16:14:14 GMT+0800">几秒前</span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			</div>
		</div>
		
	


	</body>

</html>

<script>
  //输入内容翻页
  function dj_page(){
    var page = $('.wen_page_txt').val();
    var url = $('.wen_page').attr('url');
    if(page != ''){
      window.location.href= url+page+'.html';
    }
  }

  //唤起form多选框
  layui.use(['layer','form', 'layedit', 'laydate'], function() {
    var form = layui.form,
        layer = layui.layer,
        layedit = layui.layedit,
        laydate = layui.laydate;
  });

</script>

						


<script>
$(function () {
    $('#container').highcharts({
        data: {
            table: 'datatable'
        },
        chart: {
            type: 'column'
        },
        title: {
            text: '统计表'
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: '个',
                rotation: 0
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + '</b><br/>' +
                    this.point.y + ' 个' + this.point.name.toLowerCase();
            }
        }
    });
});

</script>