<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<title>表单</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="format-detection" content="telephone=no">

		<link rel="stylesheet" href="/ygcrafts/public/plugins/layui_new/css/layui.css" media="all" />
		<link rel="stylesheet" href="/ygcrafts/public/plugins/font-awesome/css/font-awesome.min.css">
		<!--  -->
		<link rel="stylesheet" href="/ygcrafts/public/css/comment.css" media="all">
		<link rel="stylesheet" href="/ygcrafts/public/css/table.css" />
		<link rel="stylesheet" href="/ygcrafts/public/css/main.css" />


		<script type="text/javascript" src="/ygcrafts/public/js/jquery-3.1.1.js"></script>
		<script type="text/javascript" src="/ygcrafts/public/plugins/layui_new/layui.js"></script>
		<!-- 图表 -->
		<script type="text/javascript" src="/ygcrafts/public/Highcharts/5.0.6/js/highcharts.js"></script>
		<script type="text/javascript" src="/ygcrafts/public/Highcharts/5.0.6/js/modules/data.js"></script>
		<!-- 公共js -->
		<script type="text/javascript" src="/ygcrafts/public/js/common.js"></script>
	</head>

	<body>



<div class="table_box">
  <!-- 操作按钮 -->
  <div class="layui-input-inline" >
    <button class="layui-btn layui-btn-sm layui-btn-normal"  onclick="doedit(1)">
      <i class="layui-icon">&#xe608;</i>
      添加分类
    </button>
   
  </div>


  <!-- 表格 -->
  <table class="layui-table layui-table-wen" >
    <colgroup>
      <col width="100">
      <col>
      <col>
      <col>

     
      <col width="200">
      <col width="120">
    </colgroup>
    <thead>
      <tr>
        <th>ID</th>
        <th>分类名称</th>
        <th>分类级别</th>
        <th>状态</th>
        <th>添加时间</th>
        
        <th>操作</th>
      </tr> 
    </thead>
    <tbody>
      <?php if(is_array($list)): foreach($list as $k=>$v): ?><tr>
          <td><?php echo ($v["id"]); ?></td>
          <td><?php echo ($v["name"]); ?></td>
          <td>
            <?php if($v["pid"] == 0): ?>一级分类
            <?php else: ?>
              二级分类<?php endif; ?>
          </td>
          <td>
            <?php if($v["status"] == 1): ?>开启
            <?php else: ?>
              关闭<?php endif; ?>

          </td>
          <td><?php echo ($v["ctime"]); ?></td>
          <td>
            <a  class="wen_a" onclick="doedit(2,<?php echo ($v["id"]); ?>,'<?php echo ($v["name"]); ?>','<?php echo ($v["pid"]); ?>','<?php echo ($v["status"]); ?>')">编辑</a> 
            <div class="wen_a_fenge"></div>
            <a  class="wen_a" onclick="del(<?php echo ($v["id"]); ?>)">删除</a> 
          </td>
        </tr>
        <!-- 循环中循环 -->
        <?php if(is_array($v['data'])): foreach($v['data'] as $kk=>$vv): ?><tr>
          <td><?php echo ($vv["id"]); ?></td>
          <td> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|—— <?php echo ($vv["name"]); ?></td>
          <td>
            <?php if($vv["pid"] == 0): ?>一级分类
            <?php else: ?>
              二级分类<?php endif; ?>
          </td>
          <td>
            <?php if($vv["status"] == 1): ?>开启
            <?php else: ?>
              关闭<?php endif; ?>
          </td>
          <td><?php echo ($vv["ctime"]); ?></td>
          <td>
            <a  class="wen_a" onclick="doedit(2,<?php echo ($vv["id"]); ?>,'<?php echo ($vv["name"]); ?>','<?php echo ($vv["pid"]); ?>','<?php echo ($v["status"]); ?>')">编辑</a> 
            <div class="wen_a_fenge"></div>
            <a  class="wen_a" onclick="del(<?php echo ($vv["id"]); ?>)">删除</a> 
          </td>
        </tr><?php endforeach; endif; endforeach; endif; ?>
      
    </tbody>
  </table>

</div>

	


	</body>

</html>

<script>
  //输入内容翻页
  function dj_page(){
    var page = $('.wen_page_txt').val();
    var url = $('.wen_page').attr('url');
    if(page != ''){
      window.location.href= url+page+'.html';
    }
  }

  //唤起form多选框
  layui.use(['layer','form', 'layedit', 'laydate'], function() {
    var form = layui.form,
        layer = layui.layer,
        layedit = layui.layedit,
        laydate = layui.laydate;
  });

</script>












<!-- ---------------------------------------------------------------- -->










<script>
//模态窗
function doedit (type,id,name,pid,status) {
  if(type == 1){
    var txt = '添加'; 
  }else{
    var txt = '编辑'; 
  }
  layui.use(['layer','form'], function() {
    var form = layui.form;
    var layer = layui.layer;

    layer.open({
      type: 1,
      title: txt,
      area: ['500px', '400px'] ,
      shadeClose: true,
      content: $('#box').html()
    })

    $('#name').val(name);
    $('#pid').val(pid);
    $('#status').val(status);
   
    $('.editId').val(id);
    $('.typeId').val(type);

    form.render();//渲染
  })
}

//确定-提交
function sub(){
  var name = $('#name').val();
  var status = $('#status').val();
  var pid = $('#pid').val();
// alert(pid);
  if(name == ''){
      layer.msg('分类名不能为空',{
        time:900  ,           //时间
      });
      return false;
  }
  if(pid == null){
      layer.msg('分类归属不能为空',{
        time:900  ,           //时间
      });
      return false;
  }
  if(status == null){
      layer.msg('状态不能为空',{
        time:900  ,           //时间
      });
      return false;
  }
  
  $.ajax({
      type:"get",
      url:"/ygcrafts/admin.php/type/add_edit",
      data:$("#add_edit").serialize(),

      success:function(data){
        // alert(data);
        returnInfo(data);

      }        
  })//ajax结束
}
//取消-提交

//删除
function del(id){

  layer.confirm('确定删除这个分类吗？', {
    btn: ['确定'], //按钮
    title: '系统提示',
    shadeClose: true
  }, function(){  //确定
    $.ajax({
        type:"get",
        url:"/ygcrafts/admin.php/type/del?id="+id,
        

        success:function(data){
          console.log(data);
          // alert(data);
          returnInfo(data);

        }        
    })//ajax结束
  })


}

</script>










<!-- ---------------------------------------------------------------- -->











<script id="box">
  <style>
    .layui-input-item{
      margin-bottom: 15px;
    }
  </style>
  <form class="layui-form" id="add_edit" action="" style="margin:15px;">
    <!-- 隐藏域 -->
    <input type="hidden" name="id" class="editId">
    <input type="hidden" name="type" class="typeId">
    <!-- 文本输入 -->
    <div class="layui-form-item" style="">
      <label class="layui-form-label"><span style="color:red">*</span> 分类名称</label>
      <div class="layui-input-inline">
        <input style="width:300px" type="text" id="name" name="name" lay-verify="name" autocomplete="off" placeholder="请输入分类名称" class="layui-input" >
      </div>
    </div>
    <!-- 下拉选框 -->
    <div class="layui-form-item">
      <label class="layui-form-label"><span style="color:red">*</span> 分类归属</label>
      <div class="layui-input-inline" style="width:300px">
        <select name="pid" id="pid" >
          <option value="0">--顶级分类--</option>
          <?php if(is_array($list)): foreach($list as $k=>$v): ?><option value="<?php echo ($v["id"]); ?>"><?php echo ($v["name"]); ?></option><?php endforeach; endif; ?>
        </select>
      </div>
    </div>  
    <!-- 下拉选框 -->
    <div class="layui-form-item">
      <label class="layui-form-label"><span style="color:red">*</span> 状态</label>
      <div class="layui-input-inline" style="width:300px">
        <select name="status" id="status" >
          <option value="1">开启</option>
          <option value="2">关闭</option>
        </select>
      </div>
    </div> 
 
    <!-- 按钮 -->
    <div class="layui-form-item" style="margin:20px auto;width:150px;">
      <a class="layui-btn" onclick="sub()">确定</a>
      <a class="layui-btn layui-btn-primary" onclick="close_()">取消</a>
    </div>
  </form>
</div>
</script>