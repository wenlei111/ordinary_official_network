<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<title>表单</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="format-detection" content="telephone=no">

		<link rel="stylesheet" href="/ygcrafts/public/plugins/layui_new/css/layui.css" media="all" />
		<link rel="stylesheet" href="/ygcrafts/public/plugins/font-awesome/css/font-awesome.min.css">
		<!--  -->
		<link rel="stylesheet" href="/ygcrafts/public/css/comment.css" media="all">
		<link rel="stylesheet" href="/ygcrafts/public/css/table.css" />
		<link rel="stylesheet" href="/ygcrafts/public/css/main.css" />


		<script type="text/javascript" src="/ygcrafts/public/js/jquery-3.1.1.js"></script>
		<script type="text/javascript" src="/ygcrafts/public/plugins/layui_new/layui.js"></script>
		<!-- 图表 -->
		<script type="text/javascript" src="/ygcrafts/public/Highcharts/5.0.6/js/highcharts.js"></script>
		<script type="text/javascript" src="/ygcrafts/public/Highcharts/5.0.6/js/modules/data.js"></script>
		<!-- 公共js -->
		<script type="text/javascript" src="/ygcrafts/public/js/common.js"></script>
	</head>

	<body>



<div class="table_box">
  <!-- 筛选 -->
  <form class="layui-form" id="search" action="/ygcrafts/admin.php/admin/list_" method="get" style="margin-bottom: 20px;">
      <div class="layui-input-inline">
        <input type="text" name="username1" lay-verify="username1" autocomplete="off" placeholder="账号搜索" class="layui-input" style="width:200px;" value="<?php echo ($username1); ?>">
      </div>
      <div class="layui-input-inline">
        <select name="group_id_" id="group_id_">
          <option value="">分组查询</option>
          <?php if(is_array($groupname)): foreach($groupname as $k=>$v): ?><option value="<?php echo ($v["id"]); ?>"><?php echo ($v["groupname"]); ?></option><?php endforeach; endif; ?>
        </select>    

      </div>
      <script type="text/javascript">
        $("#group_id_").val("<?php echo ($group_id_); ?>");
        /*function fomrReset()
        {
            document.getElementById("search").reset();
        }*/
        
      </script>

      <div class="layui-input-inline" style="margin-left: 10px;">
        <button class="layui-btn " type="submit">搜索</button>
        <button class="layui-btn layui-btn-primary" onclick="resetAll()"   style="margin-left: 00px;">重置</button>
      </div>
      
  </form>
  <!-- 操作按钮 -->
  <div class="layui-input-inline" >
    <button class="layui-btn layui-btn-sm layui-btn-normal" type="submit" onclick="doedit(1)">
      <i class="layui-icon">&#xe608;</i>
      添加
    </button>
   
  </div>
  <!-- 表格 -->
  <table class="layui-table layui-table-wen" >
    <colgroup>
      <col width="100">
      <col>
      <col>
      <col>
      <col>
      <col width="100">
      <col width="150">
    </colgroup>
    <thead>
      <tr>
        <th>ID</th>
        <th>账号</th>
        <th>账号分组</th>
        
        <th>最后登录时间</th>
        <th>说明</th>
        <th>状态</th>
        <th>操作</th>
      </tr> 
    </thead>
    <tbody>
      <?php if(is_array($list)): foreach($list as $k=>$v): ?><tr>
        <td><?php echo ($v["id"]); ?></td>
        <td><?php echo ($v["username"]); ?></td>
        <td><?php echo ($v["groupname"]); ?></td>
        
        <td>
            <?php echo ($v["logintime"]); ?>
        </td>
        <td><?php echo ($v["des"]); ?></td>
        <td>
          <?php if($v["status"] == 1): ?><span class="layui-badge-dot layui-bg-green"></span>&nbsp;&nbsp;
             开启
          <?php else: ?>
            <span class="layui-badge-dot"></span>&nbsp;&nbsp;
             关闭<?php endif; ?>
        </td>
        <td>
          <a  class="wen_a" onclick="doedit(2,<?php echo ($v["id"]); ?>,'<?php echo ($v["username"]); ?>','<?php echo ($v["des"]); ?>','<?php echo ($v["status"]); ?>','<?php echo ($v["group_id"]); ?>')">编辑</a> 
          <div class="wen_a_fenge"></div>
          <a  class="wen_a" onclick="del(<?php echo ($v["id"]); ?>)">删除</a> 
        </td>
      </tr><?php endforeach; endif; ?>
      
    </tbody>
  </table>
  <?php echo ($page); ?>

</div>

	


	</body>

</html>

<script>
  //输入内容翻页
  function dj_page(){
    var page = $('.wen_page_txt').val();
    var url = $('.wen_page').attr('url');
    if(page != ''){
      window.location.href= url+page+'.html';
    }
  }

  //唤起form多选框
  layui.use(['layer','form', 'layedit', 'laydate'], function() {
    var form = layui.form,
        layer = layui.layer,
        layedit = layui.layedit,
        laydate = layui.laydate;
  });

</script>












<!-- ---------------------------------------------------------------- -->










<script>
//模态窗
function doedit (type,id,username,des,status,group_id) {
  if(type == 1){
    var txt = '添加'; 
  }else{
    var txt = '编辑'; 
  }
  layui.use(['layer','form'], function() {
    var form = layui.form;
    var layer = layui.layer;

    layer.open({
      type: 1,
      title: txt,
      area: ['550px', '400px'] ,
      shadeClose: true,
      content: $('#box').html()
    })

    $('#username').val(username);
    $('#des').val(des);
    $('#status').val(status);
    $('#group_id').val(group_id);
    $('.editId').val(id);
    $('.typeId').val(type);

    form.render();//渲染
  })
}

//确定-提交
function sub(){
  var username = $('#username').val();
  var status = $('#status').val();
  var group_id = $('#group_id').val();

  if(username == ''){
      layer.msg('账号不能为空',{
        time:900  ,           //时间
      });
      return false;
  }
  if(group_id == null){
      layer.msg('账号分组不能为空',{
        time:900  ,           //时间
      });
      return false;
  }
  if(status == null){
      layer.msg('状态不能为空',{
        time:900  ,           //时间
      });
      return false;
  }
  
  $.ajax({
      type:"get",
      url:"/ygcrafts/admin.php/admin/add_edit",
      data:$("#add_edit").serialize(),

      success:function(data){

        returnInfo(data);

      }        
  })//ajax结束
}
//取消-提交

//删除
function del(id){
  layer.confirm('确定永久删除这个账号吗？', {
    btn: ['确定'], //按钮
    title: '系统提示',
    shadeClose: true
  }, function(){  //确定
    $.ajax({
        type:"get",
        url:"/ygcrafts/admin.php/admin/del?id="+id,
        

        success:function(data){
          console.log(data);
          // alert(data);
          returnInfo(data);

        }        
    })//ajax结束
  })


}

</script>










<!-- ---------------------------------------------------------------- -->











<script id="box">
  <style>
    .layui-input-item{
      margin-bottom: 15px;
    }
  </style>
  <form class="layui-form" id="add_edit" action="" style="margin:15px;">
    <!-- 隐藏域 -->
    <input type="hidden" name="id" class="editId">
    <input type="hidden" name="type" class="typeId">
    <!-- 文本输入 -->
    <div class="layui-form-item" style="">
      <label class="layui-form-label"><span style="color:red">*</span> 账号</label>
      <div class="layui-input-inline">
        <input type="text" id="username" name="username" lay-verify="username" autocomplete="off" placeholder="请输入账号名" class="layui-input" >
      </div>
    </div>
    <!-- 下拉选框 -->
    <div class="layui-form-item">
      <label class="layui-form-label"><span style="color:red">*</span> 账号分组</label>
      <div class="layui-input-inline">
        <select name="group_id" id="group_id">
          
          <?php if(is_array($groupname)): foreach($groupname as $k=>$v): ?><option value="<?php echo ($v["id"]); ?>"><?php echo ($v["groupname"]); ?></option><?php endforeach; endif; ?>
        </select>
      </div>
    </div>  
    <!-- 下拉选框 -->
    <div class="layui-form-item">
      <label class="layui-form-label"><span style="color:red">*</span> 状态</label>
      <div class="layui-input-inline">
        <select name="status" id="status">
          <option value="1">开启</option>
          <option value="0">关闭</option>
        </select>
      </div>
    </div> 
    <!-- 普通文本域 -->  
    <div class="layui-form-item layui-form-text">
      <label class="layui-form-label">说明</label>
      <div class="layui-input-block">
        <textarea placeholder="请输入内容" name='des' class="layui-textarea" style="width:340px" id="des"></textarea>
      </div>
    </div>
    <!-- 按钮 -->
    <div class="layui-form-item" style="margin:20px auto;width:150px;">
      <a class="layui-btn" onclick="sub()">确定</a>
      <a class="layui-btn layui-btn-primary" onclick="close_()">取消</a>
    </div>
  </form>
</div>
</script>