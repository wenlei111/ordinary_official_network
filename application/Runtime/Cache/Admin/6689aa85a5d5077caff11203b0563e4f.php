<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<title>表单</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="format-detection" content="telephone=no">

		<link rel="stylesheet" href="/bg_thinkphp3/public/plugins/layui_new/css/layui.css" media="all" />
		<link rel="stylesheet" href="/bg_thinkphp3/public/plugins/font-awesome/css/font-awesome.min.css">
		<!--  -->
		<link rel="stylesheet" href="/bg_thinkphp3/public/css/comment.css" media="all">
		<link rel="stylesheet" href="/bg_thinkphp3/public/css/table.css" />
		<link rel="stylesheet" href="/bg_thinkphp3/public/css/main.css" />


		<script type="text/javascript" src="/bg_thinkphp3/public/js/jquery-3.1.1.js"></script>
		<script type="text/javascript" src="/bg_thinkphp3/public/plugins/layui_new/layui.js"></script>
		<!-- 图表 -->
		<script type="text/javascript" src="/bg_thinkphp3/public/Highcharts/5.0.6/js/highcharts.js"></script>
		<script type="text/javascript" src="/bg_thinkphp3/public/Highcharts/5.0.6/js/modules/data.js"></script>
		<!-- 公共js -->
		<script type="text/javascript" src="/bg_thinkphp3/public/js/common.js"></script>
	</head>

	<body>

	<style>
		li{
			height:30px;line-height:30px;vertical-align: middle;
			padding-left: 10px;
		}
		li:hover{
			background: #ccc;
		}
	
        [v-cloak]{
            display: none;
        }
    
	</style>
	<div class="table_box">
		<div style="margin: 15px;">
			<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
				<legend>vue小功能</legend>
			</fieldset>
			<form class="layui-form" >
				<div class="layui-form-item">
					<label class="layui-form-label" style="width:120px" >vue模拟百度搜索</label>
					<div class="layui-input-block" style="    margin-left: 150px;">
						<input type="text" name="web_name" id="web_name"  placeholder="请输入搜索内容" class="layui-input"  v-on:keyup='jsonp()' v-model='msg'>
					</div>
					<div style="margin-left: 150px;" >
						<ul style="border:1px solid #ccc" v-show='arr.length!=0' v-cloak>
							<li v-for='(val,key) in arr'  :name='key' @click='arrIndex=key;chenge()'>
								{{val}} 
							</li>
						</ul>
					</div>
				</div>
			</form>
		</div>
	</div>

	


	</body>

</html>

<script>
  //输入内容翻页
  function dj_page(){
    var page = $('.wen_page_txt').val();
    var url = $('.wen_page').attr('url');
    if(page != ''){
      window.location.href= url+page+'.html';
    }
  }

  //唤起form多选框
  layui.use(['layer','form', 'layedit', 'laydate'], function() {
    var form = layui.form,
        layer = layui.layer,
        layedit = layui.layedit,
        laydate = layui.laydate;
  });

</script>

<!-- -------------------------------------------------------------------------------- -->

<script type="text/javascript" src="/bg_thinkphp3/public/vue/vue.min.js"></script>
<script type="text/javascript" src="/bg_thinkphp3/public/vue/vue-resource.js"></script>
<script>
window.onload=function  () {
	new Vue({
		el:'.table_box',
		data:{
			msg:'',
			arr:[],
			arrIndex: ''
		},
		methods:{
			chenge:function(){
				// alert(11)
				// alert(ind)
				this.msg = this.arr[this.arrIndex]
				this.arr = ''
			},
			jsonp:function(){
				this.$http.jsonp('https://sp0.baidu.com/5a1Fazu8AA54nxGko9WTAnF6hhy/su',{wd:this.msg},{jsonp:'cb'}).then(function(res){
					
					this.arr=res.data.s
				},function(){

				});
			}
			
		}
	})

}
</script>