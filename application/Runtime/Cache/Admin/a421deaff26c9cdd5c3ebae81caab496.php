<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<title>表单</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="format-detection" content="telephone=no">

		<link rel="stylesheet" href="/ygcrafts/public/plugins/layui_new/css/layui.css" media="all" />
		<link rel="stylesheet" href="/ygcrafts/public/plugins/font-awesome/css/font-awesome.min.css">
		<!--  -->
		<link rel="stylesheet" href="/ygcrafts/public/css/comment.css" media="all">
		<link rel="stylesheet" href="/ygcrafts/public/css/table.css" />
		<link rel="stylesheet" href="/ygcrafts/public/css/main.css" />


		<script type="text/javascript" src="/ygcrafts/public/js/jquery-3.1.1.js"></script>
		<script type="text/javascript" src="/ygcrafts/public/plugins/layui_new/layui.js"></script>
		<!-- 图表 -->
		<script type="text/javascript" src="/ygcrafts/public/Highcharts/5.0.6/js/highcharts.js"></script>
		<script type="text/javascript" src="/ygcrafts/public/Highcharts/5.0.6/js/modules/data.js"></script>
		<!-- 公共js -->
		<script type="text/javascript" src="/ygcrafts/public/js/common.js"></script>
	</head>

	<body>


<!-- 判断是列表还是添加修改 -->


<!-- ----------------------------list_---------------------------------- -->
<?php if(ACTION_NAME == 'list_'): ?><div class="table_box">
  <!-- 筛选 -->
  <form class="layui-form" id="search" action="/ygcrafts/admin.php/admin/list_" method="get" style="margin-bottom: 20px;">
      <div class="layui-input-inline">
        <input type="text" name="username1" lay-verify="username1" autocomplete="off" placeholder="账号搜索" class="layui-input" style="width:200px;" value="<?php echo ($username1); ?>">
      </div>
      <div class="layui-input-inline">
        <select name="group_id_" id="group_id_">
          <option value="">分组查询</option>
          <?php if(is_array($groupname)): foreach($groupname as $k=>$v): ?><option value="<?php echo ($v["id"]); ?>"><?php echo ($v["groupname"]); ?></option><?php endforeach; endif; ?>
        </select>    

      </div>
      <script type="text/javascript">
        $("#group_id_").val("<?php echo ($group_id_); ?>");
      </script>

      <div class="layui-input-inline" style="margin-left: 10px;">
        <button class="layui-btn " type="submit">搜索</button>
        <button class="layui-btn layui-btn-primary" onclick="resetAll()"   style="margin-left: 00px;">重置</button>
      </div>
      
  </form>

  <!-- 操作按钮 -->
  <div class="layui-input-inline" >
    <button class="layui-btn layui-btn-sm layui-btn-normal"  onclick="doedit(1)">
      <i class="layui-icon">&#xe608;</i>
      添加商品
    </button>
   
  </div>

  <!-- 表格 -->
  <table class="layui-table layui-table-wen" >
    <colgroup>
      <col width="100">
      <col>
      <col>

      <col>
      <col>
      <col>

      <col width="100">
      <col width="120">
    </colgroup>
    <thead>
      <tr>
        <th>ID</th>
        <th>商品名</th>
        <th>一级分类</th>

        <th>二级分类</th>
        <th>封面图片</th>
        <th>添加时间</th>
        
       
        <th>状态</th>
        <th>操作</th>
      </tr> 
    </thead>
    <tbody>
      <?php if(is_array($list)): foreach($list as $k=>$v): ?><tr>
        <td><?php echo ($v["id"]); ?></td>
        <td><?php echo ($v["title"]); ?></td>
        <td><?php echo ($v["ppid"]); ?></td>
        
        <td><?php echo ($v["pid"]); ?></td>
        <td>
        
          <img src="<?php echo ($v["pic_fm"]); ?>" style="height:40px">
        </td>
        <td><?php echo ($v["ctime"]); ?></td>
        
       
        <td>
          <?php if($v["status"] == 1): ?><span class="layui-badge-dot layui-bg-green"></span>&nbsp;&nbsp;
             显示
          <?php else: ?>
            <span class="layui-badge-dot"></span>&nbsp;&nbsp;
             不显示<?php endif; ?>
        </td>
        <td>
          <a  class="wen_a"  onclick="doedit(2,<?php echo ($v["id"]); ?>)">编辑</a> 
          <div class="wen_a_fenge"></div>
          <a  class="wen_a" onclick="del(<?php echo ($v["id"]); ?>)">删除</a> 
        </td>
      </tr><?php endforeach; endif; ?>
      
    </tbody>
  </table>
  <?php echo ($page); ?>

</div>




 <!-- -----------------------------add_edit--------------------------------- -->

<?php else: ?>

<div class="table_box">
  <!-- 筛选 -->
  <form class="layui-form" >
    <fieldset class="layui-elem-field layui-field-title">
      <legend>商品详情<?php echo ($ids); ?></legend>
      <div class="layui-field-box">
        
        <!-- <input type="hidden" name="id_edit" id="id_edit"  > -->

        <div class="layui-form-item" style="margin-top:20px;">
          <label class="layui-form-label"><span style="color:red">*</span> 商品名称</label>
          <div class="layui-input-inline">
            <input type="title" name="title" id="title" autocomplete="off"  class="layui-input" value="<?php echo ($list_xg["title"]); ?>"  title="禁止输入">
          </div>
         
        </div>



        <div class="layui-form-item">
          <label class="layui-form-label">封面图</label>
          <!-- 显示头像 -->
          <div class="layui-input-inline" style="width:100px;">
            <div class="layui-upload">
              <div class="layui-upload-list" style="margin: 0px 0;">
                <img class="layui-upload-img" src='<?php if($list_xg["pic_fm"] != ''): echo ($list_xg["pic_fm"]); else: ?>/ygcrafts/public/images/avater/mr.png<?php endif; ?>' id="demo" style="border:1px solid #ccc;width:100px;">
                <p id="demoText"></p>
              </div>
            </div>  
          </div>
          <!-- 上传按钮 -->
          <button type="button" class="layui-btn  layui-btn-sm layui-btn-normal" id="pic_fm">
            上传封面图
          </button>
        </div>




        <div class="layui-form-item">
          <label class="layui-form-label">内容图</label>
          <!-- 显示头像 -->
          <div class="layui-input-inline" style="width:100px;">
            <div class="layui-upload">
              <div class="layui-upload-list" style="margin: 0px 0;">
                <img class="layui-upload-img" src="/ygcrafts/public/images/avater/mr.png" id="demo1" style="border:1px solid #ccc;width:100px;">
                <p id="demoText1"></p>
              </div>
            </div>  
          </div>
          <!-- 上传按钮 -->
          <button type="button" class="layui-btn  layui-btn-sm layui-btn-normal" id="pic1">
            上传图1
          </button>

          <!-- 显示头像 -->
          <div class="layui-input-inline" style="width:100px;">
            <div class="layui-upload">
              <div class="layui-upload-list" style="margin: 0px 0;">
                <img class="layui-upload-img" src="/ygcrafts/public/images/avater/mr.png" id="demo2" style="border:1px solid #ccc;width:100px;">
                <p id="demoText2"></p>
              </div>
            </div>  
          </div>
          <!-- 上传按钮 -->
          <button type="button" class="layui-btn  layui-btn-sm layui-btn-normal" id="pic2">
            上传图2
          </button>

          <!-- 显示头像 -->
          <div class="layui-input-inline" style="width:100px;">
            <div class="layui-upload">
              <div class="layui-upload-list" style="margin: 0px 0;">
                <img class="layui-upload-img" src="/ygcrafts/public/images/avater/mr.png" id="demo3" style="border:1px solid #ccc;width:100px;">
                <p id="demoText3"></p>
              </div>
            </div>  
          </div>
          <!-- 上传按钮 -->
          <button type="button" class="layui-btn  layui-btn-sm layui-btn-normal" id="pic3">
            上传图3
          </button>
        </div>




        <div class="layui-form-item layui-form-text">
          <label class="layui-form-label">商品简介</label>
          <div class="layui-input-block">
            <textarea placeholder="请输入内容" name='des' class="layui-textarea" style="width:95%" id="des" ><?php echo ($list_xg["a_des"]); ?></textarea>
          </div>
        </div>

        <div class="layui-form-item">
          <label class="layui-form-label"><span style="color:red">*</span> 价格</label>
          <div class="layui-input-inline">
            <input type="title" name="price" id="price"  autocomplete="off"  class="layui-input" value="<?php echo ($list_xg["price"]); ?>">
          </div>
        </div>

        <div class="layui-form-item">
          <label class="layui-form-label"><span style="color:red">*</span> 商品分类</label>
          <div class="layui-input-inline">
            <select name="pid" id="pid">
              <option value="">分类选择</option>
              <?php if(is_array($group)): foreach($group as $k=>$v): ?><option value="<?php echo ($v["id"]); ?>"><?php echo ($v["name"]); ?></option><?php endforeach; endif; ?>
            </select>    
          </div>
        </div>





        

        

      </div>
    </fieldset>
  </form>
  <!--  -->
  <div class="layui-form-item">
    <div class="layui-input-block">
      <button class="layui-btn " onclick="sub()">确认</button>
    </div>
  </div><?php endif; ?>



	


	</body>

</html>

<script>
  //输入内容翻页
  function dj_page(){
    var page = $('.wen_page_txt').val();
    var url = $('.wen_page').attr('url');
    if(page != ''){
      window.location.href= url+page+'.html';
    }
  }

  //唤起form多选框
  layui.use(['layer','form', 'layedit', 'laydate'], function() {
    var form = layui.form,
        layer = layui.layer,
        layedit = layui.layedit,
        laydate = layui.laydate;
  });

</script>



<!-- -----------------------------js----------------------------------- -->



<script>
layui.use('upload', function(){
  var $ = layui.jquery
  ,upload = layui.upload;

  var uploadInst = upload.render({
    elem: '#pic_fm'
    ,url: '/ygcrafts/admin.php/api/upload'
    ,method:'post'
    // ,before: function(obj){
    //   //预读本地文件示例，不支持ie8
    //   obj.preview(function(index, file, result){
    //     $('#demo1').attr('src', result); //图片链接（base64）
    //   });
    // }
    ,done: function(res){
      // console.log(res);
      //如果上传失败
      if(res.status == 0){
        return layer.msg('上传失败');
      }else{
        $('#demo').attr('src', res.data);
      }
    }
    ,error: function(){
      //演示失败状态，并实现重传
      var demoText = $('#demoText');
      demoText.html('<span style="color: #FF5722;">上传失败</span> ');

    }
  });
});

layui.use('upload', function(){
  var $ = layui.jquery
  ,upload = layui.upload;

  var uploadInst = upload.render({
    elem: '#pic1'
    ,url: '/ygcrafts/admin.php/api/upload'
    ,method:'post'
    // ,before: function(obj){
    //   //预读本地文件示例，不支持ie8
    //   obj.preview(function(index, file, result){
    //     $('#demo1').attr('src', result); //图片链接（base64）
    //   });
    // }
    ,done: function(res){
      // console.log(res);
      //如果上传失败
      if(res.status == 0){
        return layer.msg('上传失败');
      }else{
        $('#demo1').attr('src', res.data);
      }
    }
    ,error: function(){
      //演示失败状态，并实现重传
      var demoText = $('#demoText1');
      demoText.html('<span style="color: #FF5722;">上传失败</span> ');

    }
  });
});

layui.use('upload', function(){
  var $ = layui.jquery
  ,upload = layui.upload;

  var uploadInst = upload.render({
    elem: '#pic2'
    ,url: '/ygcrafts/admin.php/api/upload'
    ,method:'post'
    // ,before: function(obj){
    //   //预读本地文件示例，不支持ie8
    //   obj.preview(function(index, file, result){
    //     $('#demo1').attr('src', result); //图片链接（base64）
    //   });
    // }
    ,done: function(res){
      // console.log(res);
      //如果上传失败
      if(res.status == 0){
        return layer.msg('上传失败');
      }else{
        $('#demo2').attr('src', res.data);
      }
    }
    ,error: function(){
      //演示失败状态，并实现重传
      var demoText = $('#demoText2');
      demoText.html('<span style="color: #FF5722;">上传失败</span> ');

    }
  });
});

layui.use('upload', function(){
  var $ = layui.jquery
  ,upload = layui.upload;

  var uploadInst = upload.render({
    elem: '#pic3'
    ,url: '/ygcrafts/admin.php/api/upload'
    ,method:'post'
    // ,before: function(obj){
    //   //预读本地文件示例，不支持ie8
    //   obj.preview(function(index, file, result){
    //     $('#demo1').attr('src', result); //图片链接（base64）
    //   });
    // }
    ,done: function(res){
      // console.log(res);
      //如果上传失败
      if(res.status == 0){
        return layer.msg('上传失败');
      }else{
        $('#demo3').attr('src', res.data);
      }
    }
    ,error: function(){
      //演示失败状态，并实现重传
      var demoText = $('#demoText3');
      demoText.html('<span style="color: #FF5722;">上传失败</span> ');

    }
  });
});

//确定-提交
function sub(){
  var title = $('#title').val();
  var pic_fm = $('#demo').attr('src');
  var pic1 = $('#demo1').attr('src');
  var pic2 = $('#demo2').attr('src');
  var pic3 = $('#demo3').attr('src');

  // alert(img);


  if(title == ''){
      layer.msg('标题不能为空',{
        time:900  ,           //时间
        offset: '100px' 
      });
      
      $('#title').css('border','1px solid #ff0000');
      var timer = setTimeout(function () {
            $('#title').css('border','1px solid #e6e6e6');
      },1000);
      return false;
  }


  
  $.ajax({
      type:"get",
      url:"/ygcrafts/admin.php/detail/add_edit?pic_fm="+pic_fm+"&pic1="+pic1+"&pic2="+pic2+"&pic3="+pic3,
      data:$(".layui-form").serialize(),

      success:function(data){
      	// alert(data);
        returnInfo(data);
      }        
  })
}

//添加修改
function doedit(type,id){
  if(type==1){
    window.location.href='/ygcrafts/admin.php/detail/add_edit';
  }else if(type==2){
    window.location.href='/ygcrafts/admin.php/detail/add_edit?ids='+id;
  }
  
}


</script>



<!-- ---------------------------------------------------------------- -->