<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<title>表单</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="format-detection" content="telephone=no">

		<link rel="stylesheet" href="/ygcrafts/public/plugins/layui_new/css/layui.css" media="all" />
		<link rel="stylesheet" href="/ygcrafts/public/plugins/font-awesome/css/font-awesome.min.css">
		<!--  -->
		<link rel="stylesheet" href="/ygcrafts/public/css/comment.css" media="all">
		<link rel="stylesheet" href="/ygcrafts/public/css/table.css" />
		<link rel="stylesheet" href="/ygcrafts/public/css/main.css" />


		<script type="text/javascript" src="/ygcrafts/public/js/jquery-3.1.1.js"></script>
		<script type="text/javascript" src="/ygcrafts/public/plugins/layui_new/layui.js"></script>
		<!-- 图表 -->
		<script type="text/javascript" src="/ygcrafts/public/Highcharts/5.0.6/js/highcharts.js"></script>
		<script type="text/javascript" src="/ygcrafts/public/Highcharts/5.0.6/js/modules/data.js"></script>
		<!-- 公共js -->
		<script type="text/javascript" src="/ygcrafts/public/js/common.js"></script>
	</head>

	<body>



<div class="table_box">
  <!-- 筛选 -->
  <!-- <form class="layui-form" action="" style="margin-bottom: 20px;">
      <div class="layui-input-inline">
        <input type="text" name="title" lay-verify="title" autocomplete="off" placeholder="角色搜索" class="layui-input" style="width:200px;">
      </div>
      <div class="layui-input-inline">
        <select name="city" >
          <option value="">分组查询</option>
          <option value="1">超级管理员</option>
          <option value="2">人事</option>
          <option value="3">财务</option>
          <option value="4">普通员工</option>
        </select>     
      </div>
      <div class="layui-input-inline" style="margin-left: 10px;">
        <button class="layui-btn " type="submit">搜索</button>
        <button class="layui-btn layui-btn-primary" type="reset" value="" style="margin-left: 00px;">重置</button>
      </div>
  </form> -->
  <!-- 操作按钮 -->
  <div class="layui-input-inline" >
    <button class="layui-btn layui-btn-sm layui-btn-normal"  onclick="doedit(1)">
      <i class="layui-icon">&#xe608;</i>
      添加角色
    </button>
   
  </div>
  <!-- 表格 -->
  <table class="layui-table layui-table-wen" >
    <colgroup>
      <col width="100">
      <col>
      <col>
      <col width="100">
      <col width="150">
    </colgroup>
    <thead>
      <tr>
        <th>ID</th>
        
        <th>角色</th>
        <th>角色描述</th>
        <th>状态</th>
      
        <th>操作</th>
      </tr> 
    </thead>
    <tbody>
      <?php if(is_array($list)): foreach($list as $k=>$v): ?><tr>
          <td><?php echo ($v["id"]); ?></td>
          
          <td><?php echo ($v["groupname"]); ?></td>
          <td><?php echo ($v["des"]); ?></td>
          <td>
            <?php if($v["status"] == 1): ?><span class="layui-badge-dot layui-bg-green"></span>&nbsp;&nbsp;
               开启
            <?php else: ?>
              <span class="layui-badge-dot"></span>&nbsp;&nbsp;
               关闭<?php endif; ?>
          </td>
          
          <td>
            <a class="wen_a" onclick="doedit(2,<?php echo ($v["id"]); ?>,'<?php echo ($v["groupname"]); ?>','<?php echo ($v["des"]); ?>','<?php echo ($v["status"]); ?>','<?php echo ($v["rule_str"]); ?>')">编辑</a> 
            <div class="wen_a_fenge"></div>
            <a class="wen_a" onclick="del()">删除</a> 
          </td>
        </tr><?php endforeach; endif; ?>
      
    </tbody>
  </table>
  <?php echo ($page); ?>

</div>

	


	</body>

</html>

<script>
  //输入内容翻页
  function dj_page(){
    var page = $('.wen_page_txt').val();
    var url = $('.wen_page').attr('url');
    if(page != ''){
      window.location.href= url+page+'.html';
    }
  }

  //唤起form多选框
  layui.use(['layer','form', 'layedit', 'laydate'], function() {
    var form = layui.form,
        layer = layui.layer,
        layedit = layui.layedit,
        laydate = layui.laydate;
  });

</script>



<!-- ---------------------------------------------------------------- -->



<script>
  //模态窗
function doedit (type,id,groupname,des,status,rule_str) {
	// console.log(rule_str);
	// alert(rule_str);
  if(type == 1){
    var txt = '添加'; 
  }else{
    var txt = '编辑'; 
  }
  layui.use(['layer','form'], function() {
    var form = layui.form;
    var layer = layui.layer;

    var index = layer.open({
      type: 1,
      title: txt,
      area: ['750px', '700px'] ,
      shadeClose: true,
      closeBtn:0,
      maxmin: true,
      content: $('#box').html()
    })
    // layer.full(index);
    $('#groupname').val(groupname);
    $('#des').val(des);
    $('#status').val(status);
    $('.editId').val(id);
    $('.typeId').val(type);
 
    form.render();//渲染
    var checkBoxArray = rule_str.split(",");  
    for(var i=0;i<checkBoxArray.length;i++){  
        /*$("input[name='qx']").each(function(){  
            if($(this).val()==checkBoxArray[i]){  
                $(this).attr("checked","checked");  
            }  
        }) */ 
  		$("#qx"+checkBoxArray[i]).attr("checked","checked"); 
    } 
    form.render();//渲染

    
  })
}


//确定-提交
function sub(){
  var groupname = $('#groupname').val();
  var status = $('#status').val();
  var des = $('#des').val();

  if(groupname == ''){
      layer.msg('账号不能为空',{
        time:900  ,           //时间
      });
      return false;
  }

  if(status == null){
      layer.msg('状态不能为空',{
        time:900  ,           //时间
      });
      return false;
  }
  
  $.ajax({
      type:"get",
      url:"/ygcrafts/admin.php/matchlist/add_edit",
      data:$("#add_edit").serialize(),

      success:function(data){
      	// alert(data);
        returnInfo(data);

      }        
  })//ajax结束
}
//删除
function del(id){
  layer.confirm('角色不能删除，如需删除，请联系后台', {
    btn: ['确定'], //按钮
    title: '系统提示',
    shadeClose: true
  }, function(){  //确定
    layer.closeAll();
  })


}
</script>



<!-- ---------------------------------------------------------------- -->



<script id="box">
  <style>
    .layui-input-item{
      margin-bottom: 15px;
    }
  </style>
  <form class="layui-form" id="add_edit" action="" style="margin:15px;">
    <!-- 隐藏域 -->
    <input type="hidden" name="id" class="editId">
    <input type="hidden" name="type" class="typeId">


    <fieldset class="layui-elem-field">
      <legend style='color:#009688;font-size:14px'>基本信息</legend>
      <div class="layui-field-box">

      <div class="layui-form-item" style="">
        <!-- 文本输入 -->
        <label class="layui-form-label"><span style="color:red">*</span> 角色名</label>
        <div class="layui-input-inline">
          <input type="text" id="groupname" name="groupname" placeholder="请输入账号名" class="layui-input" >
        </div>
        <!-- 下拉选框 -->
        <label class="layui-form-label"><span style="color:red">*</span> 状态</label>
        <div class="layui-input-inline">
          <select name="status" id="status">
            <option value="1">开启</option>
            <option value="0">关闭</option>
          </select>
        </div>
      </div> 

      <!-- 普通文本域 -->  
      <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">角色描述</label>
        <div class="layui-input-block">
          <textarea placeholder="请输入内容" name='des' class="layui-textarea" style="width:90%;height:50px;" id="des"></textarea>
        </div>
      </div>


      </div>
    </fieldset>

    <fieldset class="layui-elem-field">
      <legend style='color:#009688;font-size:14px'>角色权限</legend>
      <div class="layui-field-box">

      <!-- 多选框 -->
      <?php if(is_array($qx_list)): foreach($qx_list as $k=>$v): ?><div class="layui-form-item">
          <label class="layui-form-label"><?php echo ($v["remark"]); ?></label>
          <div class="layui-input-block">

          	<?php if(is_array($v['data'])): foreach($v['data'] as $kk=>$vv): ?><input type="checkbox" id="qx<?php echo ($vv["id"]); ?>"  name="qx[<?php echo ($vv["id"]); ?>]" title="<?php echo ($vv["jurisdiction"]); ?>" lay-skin="primary"><?php endforeach; endif; ?>

          </div>
        </div><?php endforeach; endif; ?>

      </div>
    </fieldset>


    <!-- 按钮 -->
    <div class="layui-form-item" style="margin:20px auto;width:150px;">
      <a class="layui-btn" onclick="sub()">确定</a>
      <a class="layui-btn layui-btn-primary" onclick='close_()'>取消</a>
    </div>
  </form>
</div>
</script>