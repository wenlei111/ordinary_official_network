<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<title>表单</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="format-detection" content="telephone=no">

		<link rel="stylesheet" href="/ygcrafts/public/plugins/layui_new/css/layui.css" media="all" />
		<link rel="stylesheet" href="/ygcrafts/public/plugins/font-awesome/css/font-awesome.min.css">
		<!--  -->
		<link rel="stylesheet" href="/ygcrafts/public/css/comment.css" media="all">
		<link rel="stylesheet" href="/ygcrafts/public/css/table.css" />
		<link rel="stylesheet" href="/ygcrafts/public/css/main.css" />


		<script type="text/javascript" src="/ygcrafts/public/js/jquery-3.1.1.js"></script>
		<script type="text/javascript" src="/ygcrafts/public/plugins/layui_new/layui.js"></script>
		<!-- 图表 -->
		<script type="text/javascript" src="/ygcrafts/public/Highcharts/5.0.6/js/highcharts.js"></script>
		<script type="text/javascript" src="/ygcrafts/public/Highcharts/5.0.6/js/modules/data.js"></script>
		<!-- 公共js -->
		<script type="text/javascript" src="/ygcrafts/public/js/common.js"></script>
	</head>

	<body>


<!-- 用户资料 -->

<div class="table_box">
  <!-- 筛选 -->
  <form class="layui-form" >
    <fieldset class="layui-elem-field layui-field-title">
      <legend>用户资料设置</legend>
      <div class="layui-field-box">


        <div class="layui-form-item" style="margin-top:20px;">
          <label class="layui-form-label"><span style="color:red">*</span> 我的角色</label>
          <div class="layui-input-inline">
            <input type="title" name="groupname" id="groupname" autocomplete="off"  class="layui-input" value="<?php echo ($info["groupname"]); ?>" disabled title="禁止输入">
          </div>
          <div class="layui-form-mid layui-word-aux">角色不可修改哦~</div>
        </div>

        <div class="layui-form-item">
          <label class="layui-form-label"><span style="color:red">*</span> 账号名</label>
          <div class="layui-input-inline">
            <input type="title" name="username" id="username"  autocomplete="off"  class="layui-input" value="<?php echo ($info["username"]); ?>">
          </div>
          <div class="layui-form-mid layui-word-aux">登录后台用的啦</div>
        </div>

        <div class="layui-form-item">
          <label class="layui-form-label"><span style="color:red">*</span> 昵称</label>
          <div class="layui-input-inline">
            <input type="title" name="nickname" id="nickname"  autocomplete="off"  class="layui-input" value="<?php echo ($info["nickname"]); ?>">
          </div>
          <div class="layui-form-mid layui-word-aux">显示的称谓</div>
        </div>

        <div class="layui-form-item">
          <label class="layui-form-label"><span style="color:red">*</span> 手机</label>
          <div class="layui-input-inline">
            <input type="title" name="phone" id="phone"  autocomplete="off"  class="layui-input" value="<?php echo ($info["phone"]); ?>">
          </div>
        </div>

        <div class="layui-form-item">
          <label class="layui-form-label"><span style="color:red">*</span> 邮箱</label>
          <div class="layui-input-inline">
            <input type="title" name="email" id="email"  autocomplete="off"  class="layui-input" value="<?php echo ($info["email"]); ?>"> 
          </div>
        </div>


        <div class="layui-form-item">
          <label class="layui-form-label">头像</label>
          <!-- 显示头像 -->
          <div class="layui-input-inline" style="width:100px;">
            <div class="layui-upload">
              <div class="layui-upload-list" style="margin: 0px 0;">
                <img class="layui-upload-img" src="/ygcrafts/public/images/<?php echo ($info["avater"]); ?>" id="demo1" style="border:1px solid #ccc;width:100px;">
                <p id="demoText"></p>
              </div>
            </div>  
          </div>
          <!-- 上传按钮 -->
          <button type="button" class="layui-btn  layui-btn-sm layui-btn-normal" id="images">
            上传头像
          </button>

        </div>

        <div class="layui-form-item layui-form-text">
          <label class="layui-form-label">备注</label>
          <div class="layui-input-block">
            <textarea placeholder="请输入内容" name='des' class="layui-textarea" style="width:95%" id="des" ><?php echo ($info["a_des"]); ?></textarea>
          </div>
        </div>

      </div>
    </fieldset>
  </form>
  <!--  -->
  <div class="layui-form-item">
    <div class="layui-input-block">
      <button class="layui-btn " onclick="sub()">确认修改</button>
    </div>
  </div>


	


	</body>

</html>

<script>
  //输入内容翻页
  function dj_page(){
    var page = $('.wen_page_txt').val();
    var url = $('.wen_page').attr('url');
    if(page != ''){
      window.location.href= url+page+'.html';
    }
  }

  //唤起form多选框
  layui.use(['layer','form', 'layedit', 'laydate'], function() {
    var form = layui.form,
        layer = layui.layer,
        layedit = layui.layedit,
        laydate = layui.laydate;
  });

</script>



<!-- ---------------------------------------------------------------- -->



<script>
layui.use('upload', function(){
  var $ = layui.jquery
  ,upload = layui.upload;

  var uploadInst = upload.render({
    elem: '#images'
    ,url: '/ygcrafts/admin.php/api/upload'
    ,method:'post'
    // ,before: function(obj){
    //   //预读本地文件示例，不支持ie8
    //   obj.preview(function(index, file, result){
    //     $('#demo1').attr('src', result); //图片链接（base64）
    //   });
    // }
    ,done: function(res){
      // console.log(res);
      //如果上传失败
      if(res.status == 0){
        return layer.msg('上传失败');
      }else{
        $('#demo1').attr('src', res.data);
      }
    }
    ,error: function(){
      //演示失败状态，并实现重传
      var demoText = $('#demoText');
      demoText.html('<span style="color: #FF5722;">上传失败</span> ');

    }
  });
});



//确定-提交
function sub(){
  var username = $('#username').val();
  var nickname = $('#nickname').val();
  var phone = $('#phone').val();
  var email = $('#email').val();
  var img = $('#demo1').attr('src');
  // alert(img);


  if(username == ''){
      layer.msg('用户名不能为空',{
        time:900  ,           //时间
        offset: '100px' 
      });
      
      $('#username').css('border','1px solid #ff0000');
      var timer = setTimeout(function () {
            $('#username').css('border','1px solid #e6e6e6');
      },1000);
      return false;
  }

  if(nickname == ''){
      layer.msg('昵称不能为空',{
        time:900  ,           //时间
        offset: '100px'
      });
      
      $('#nickname').css('border','1px solid #ff0000');
      var timer = setTimeout(function () {
            $('#nickname').css('border','1px solid #e6e6e6');
      },1000);
      return false;
  }

    if(phone == ''){
      layer.msg('手机不能为空',{
        time:900  ,           //时间
        offset: '100px'
      });
      $('#phone').css('border','1px solid #ff0000');
      var timer = setTimeout(function () {
            $('#phone').css('border','1px solid #e6e6e6');
      },1000);
      return false;
  }
  if(email == ''){
      layer.msg('邮箱不能为空',{
        time:900  ,           //时间
        offset: '100px'
      });
      $('#email').css('border','1px solid #ff0000');
      var timer = setTimeout(function () {
            $('#email').css('border','1px solid #e6e6e6');
      },1000);
      return false;
  }

  
  $.ajax({
      type:"get",
      url:"/ygcrafts/admin.php/admininfo/edit?avater="+img,
      data:$(".layui-form").serialize(),

      success:function(data){
      	// alert(data);
        returnInfo(data);
      }        
  })
}


</script>



<!-- ---------------------------------------------------------------- -->