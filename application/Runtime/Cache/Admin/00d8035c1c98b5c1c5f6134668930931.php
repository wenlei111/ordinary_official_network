<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<title>表单</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="format-detection" content="telephone=no">

		<link rel="stylesheet" href="/ygcrafts/public/plugins/layui_new/css/layui.css" media="all" />
		<link rel="stylesheet" href="/ygcrafts/public/plugins/font-awesome/css/font-awesome.min.css">
		<!--  -->
		<link rel="stylesheet" href="/ygcrafts/public/css/comment.css" media="all">
		<link rel="stylesheet" href="/ygcrafts/public/css/table.css" />
		<link rel="stylesheet" href="/ygcrafts/public/css/main.css" />


		<script type="text/javascript" src="/ygcrafts/public/js/jquery-3.1.1.js"></script>
		<script type="text/javascript" src="/ygcrafts/public/plugins/layui_new/layui.js"></script>
		<!-- 图表 -->
		<script type="text/javascript" src="/ygcrafts/public/Highcharts/5.0.6/js/highcharts.js"></script>
		<script type="text/javascript" src="/ygcrafts/public/Highcharts/5.0.6/js/modules/data.js"></script>
		<!-- 公共js -->
		<script type="text/javascript" src="/ygcrafts/public/js/common.js"></script>
	</head>

	<body>


	<div class="table_box">
		<div style="margin: 15px;">
			<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
				<legend>网站设置</legend>
			</fieldset>

			<form class="layui-form" >
				<div class="layui-form-item">
					<label class="layui-form-label">网站名称</label>
					<div class="layui-input-block">
						<input type="text" name="web_name" id="web_name"  placeholder="请输入网站名称" class="layui-input" value="<?php echo ($info[0]["value"]); ?>">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">首页title标题</label>
					<div class="layui-input-block">
						<input type="text" name="title"  id="title" placeholder="请输入标题"  class="layui-input" value="<?php echo ($info[1]["value"]); ?>">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">META关键词</label>
					<div class="layui-input-block">
						<textarea placeholder="多个关键词用英文逗号分割" name="mete_keywords" id="mete_keywords" class="layui-textarea"><?php echo ($info[2]["value"]); ?></textarea>
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">META描述</label>
					<div class="layui-input-block">
						<textarea placeholder="添加MATA描述" name="mete_description" id="mete_description"  class="layui-textarea"><?php echo ($info[3]["value"]); ?></textarea>
					</div>
				</div>

				<div class="layui-form-item layui-form-text">
					<label class="layui-form-label">版权/备案</label>
					<div class="layui-input-block">
						<textarea placeholder="请输入版权/备案信息"  name="beian" id="beian"  class="layui-textarea"><?php echo ($info[4]["value"]); ?></textarea>
					</div>
				</div>
			</form>

			<div class="layui-form-item">
				<div class="layui-input-block">
					<button class="layui-btn" onclick="sub()">确认修改</button>
					
				</div>
			</div>

		</div>
	</div>

	


	</body>

</html>

<script>
  //输入内容翻页
  function dj_page(){
    var page = $('.wen_page_txt').val();
    var url = $('.wen_page').attr('url');
    if(page != ''){
      window.location.href= url+page+'.html';
    }
  }

  //唤起form多选框
  layui.use(['layer','form', 'layedit', 'laydate'], function() {
    var form = layui.form,
        layer = layui.layer,
        layedit = layui.layedit,
        laydate = layui.laydate;
  });

</script>



<!-- -------------------------------------------------------------------------------- -->


<script>

//确定-提交
function sub(){
	
	var web_name = $('#web_name').val();
	var title = $('#title').val();
	var mete_keywords = $('#mete_keywords').val();
	var mete_description = $('#mete_description').val();
	var beian = $('#beian').val();


	$.ajax({
	  type:"get",
	  url:"/ygcrafts/admin.php/webinfo/edit",
	  data:$(".layui-form").serialize(),
	  success:function(data){
	  	// alert(data);
	  	// console.log(data);
	    returnInfo(data);

	  }        
	})//ajax结束
}

</script>