<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>

	<head>
		<meta charset="utf-8">
		<title>表单</title>
		<meta name="renderer" content="webkit">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="format-detection" content="telephone=no">

		<link rel="stylesheet" href="/ygcrafts/public/plugins/layui_new/css/layui.css" media="all" />
		<link rel="stylesheet" href="/ygcrafts/public/plugins/font-awesome/css/font-awesome.min.css">
		<!--  -->
		<link rel="stylesheet" href="/ygcrafts/public/css/comment.css" media="all">
		<link rel="stylesheet" href="/ygcrafts/public/css/table.css" />
		<link rel="stylesheet" href="/ygcrafts/public/css/main.css" />


		<script type="text/javascript" src="/ygcrafts/public/js/jquery-3.1.1.js"></script>
		<script type="text/javascript" src="/ygcrafts/public/plugins/layui_new/layui.js"></script>
		<!-- 图表 -->
		<script type="text/javascript" src="/ygcrafts/public/Highcharts/5.0.6/js/highcharts.js"></script>
		<script type="text/javascript" src="/ygcrafts/public/Highcharts/5.0.6/js/modules/data.js"></script>
		<!-- 公共js -->
		<script type="text/javascript" src="/ygcrafts/public/js/common.js"></script>
	</head>

	<body>



<div class="table_box">
  <!-- 筛选 -->
  <form class="layui-form" style="margin-bottom: 20px;">
    <fieldset class="layui-elem-field layui-field-title">
      <legend>修改密码</legend>
      <div class="layui-field-box">


        <div class="layui-form-item" style="margin-top:20px;">
          <label class="layui-form-label">当前密码</label>
          <div class="layui-input-inline">
            <input type="password" name="password_old" id="password_old" autocomplete="off"  class="layui-input">
          </div>
        </div>

        <div class="layui-form-item">
          <label class="layui-form-label">新密码</label>
          <div class="layui-input-inline">
            <input type="password" name="password_new" id="password_new"  autocomplete="off"  class="layui-input">
          </div>
          <div class="layui-form-mid layui-word-aux">请填写6到12位密码</div>
        </div>

        <div class="layui-form-item">
          <label class="layui-form-label">确认新密码</label>
          <div class="layui-input-inline">
            <input type="password" name="password_new2" id="password_new2"  autocomplete="off"  class="layui-input">
          </div>

        </div>

      </div>
    </fieldset>
  </form>
  <!--  -->
  <div class="layui-form-item">
    <div class="layui-input-block">
      <button class="layui-btn " onclick="sub()">确认修改</button>
    </div>
  </div>


	


	</body>

</html>

<script>
  //输入内容翻页
  function dj_page(){
    var page = $('.wen_page_txt').val();
    var url = $('.wen_page').attr('url');
    if(page != ''){
      window.location.href= url+page+'.html';
    }
  }

  //唤起form多选框
  layui.use(['layer','form', 'layedit', 'laydate'], function() {
    var form = layui.form,
        layer = layui.layer,
        layedit = layui.layedit,
        laydate = layui.laydate;
  });

</script>



<!-- ---------------------------------------------------------------- -->



<script>

//确定-提交
function sub(){
  var password_old = $('#password_old').val();
  var password_new = $('#password_new').val();
  var password_new2 = $('#password_new2').val();

  if(password_old == ''){
      layer.msg('当前密码不能为空',{
        time:900  ,           //时间
        offset: '100px' 
      });
      
      $('#password_old').css('border','1px solid #ff0000');
      var timer = setTimeout(function () {
            $('#password_old').css('border','1px solid #e6e6e6');
      },1000);
      return false;
  }

  if(password_new == ''){
      layer.msg('新密码不能为空',{
        time:900  ,           //时间
        offset: '100px'
      });
      
      $('#password_new').css('border','1px solid #ff0000');
      var timer = setTimeout(function () {
            $('#password_new').css('border','1px solid #e6e6e6');
      },1000);
      return false;
  }

    if(password_new2 == ''){
      layer.msg('确认新密码不能为空',{
        time:900  ,           //时间
        offset: '100px'
      });
      $('#password_new2').css('border','1px solid #ff0000');
      var timer = setTimeout(function () {
            $('#password_new2').css('border','1px solid #e6e6e6');
      },1000);
      return false;
  }
  if(password_new != password_new2){
      layer.msg('新密码前后输入不一致',{
        time:900  ,           //时间
        offset: '100px'
      });
      
      $('#password_new').css('border','1px solid #ff0000');
      $('#password_new2').css('border','1px solid #ff0000');
      var timer = setTimeout(function () {
            $('#password_new').css('border','1px solid #e6e6e6');
            $('#password_new2').css('border','1px solid #e6e6e6');
      },1000);
      return false;
  }
  
  $.ajax({
      type:"get",
      url:"/ygcrafts/admin.php/password/edit",
      data:"password_old="+password_old+"&password_new="+password_new,

      success:function(data){
      	// alert(data);
        returnInfo(data);

      }        
  })//ajax结束
}

</script>



<!-- ---------------------------------------------------------------- -->



<script id="box">
  <style>
    .layui-input-item{
      margin-bottom: 15px;
    }
  </style>
  <form class="layui-form" id="add_edit" action="" style="margin:15px;">
    <!-- 隐藏域 -->
    <input type="hidden" name="id" class="editId">
    <input type="hidden" name="type" class="typeId">


    <fieldset class="layui-elem-field">
      <legend style='color:#009688;font-size:14px'>基本信息</legend>
      <div class="layui-field-box">

      <div class="layui-form-item" style="">
        <!-- 文本输入 -->
        <label class="layui-form-label"><span style="color:red">*</span> 角色名</label>
        <div class="layui-input-inline">
          <input type="text" id="groupname" name="groupname" placeholder="请输入账号名" class="layui-input" >
        </div>
        <!-- 下拉选框 -->
        <label class="layui-form-label"><span style="color:red">*</span> 状态</label>
        <div class="layui-input-inline">
          <select name="status" id="status">
            <option value="1">开启</option>
            <option value="0">关闭</option>
          </select>
        </div>
      </div> 

      <!-- 普通文本域 -->  
      <div class="layui-form-item layui-form-text">
        <label class="layui-form-label">角色描述</label>
        <div class="layui-input-block">
          <textarea placeholder="请输入内容" name='des' class="layui-textarea" style="width:90%;height:50px;" id="des"></textarea>
        </div>
      </div>


      </div>
    </fieldset>

    <fieldset class="layui-elem-field">
      <legend style='color:#009688;font-size:14px'>角色权限</legend>
      <div class="layui-field-box">

      <!-- 多选框 -->
      <?php if(is_array($qx_list)): foreach($qx_list as $k=>$v): ?><div class="layui-form-item">
          <label class="layui-form-label"><?php echo ($v["remark"]); ?></label>
          <div class="layui-input-block">

          	<?php if(is_array($v['data'])): foreach($v['data'] as $kk=>$vv): ?><input type="checkbox" id="qx<?php echo ($vv["id"]); ?>"  name="qx[<?php echo ($vv["id"]); ?>]" title="<?php echo ($vv["jurisdiction"]); ?>" lay-skin="primary"><?php endforeach; endif; ?>

	            
            
          </div>
        </div><?php endforeach; endif; ?>

      </div>
    </fieldset>


    <!-- 按钮 -->
    <div class="layui-form-item" style="margin:20px auto;width:150px;">
      <a class="layui-btn" onclick="sub()">确定</a>
      <button class="layui-btn layui-btn-primary" >取消</button>
    </div>
  </form>
</div>
</script>