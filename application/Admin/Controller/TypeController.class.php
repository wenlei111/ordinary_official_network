<?php
/*管理员列表*/
namespace Admin\Controller;

use Think\Page;//分页类

class TypeController extends BaseController {

    public function list_(){
    	#+----------------------------------
    	# * 分页
    	#+----------------------------------
        $list = M()->query("select * from type where pid = 0 and status != 3");
        
        foreach ($list as $k => $v) {
            $list[$k]['ctime'] =  date('Y-m-d h:i:s',$list[$k]['ctime']); 
            $list[$k]['data'] = M()->query("select * from type where pid = $v[id] and status != 3");
            foreach ($list[$k]['data'] as $kk => $vv) {
                $list[$k]['data'][$kk]['ctime'] =date('Y-m-d h:i:s',$list[$k]['data'][$kk]['ctime']);
            }
            
        }
        // print_r($list);
        $this -> assign( "list", $list); 
        #+----------------------------------
        # *  获取分类
        #+----------------------------------
        /*$groupname = M()->query("select * from admingroup");
        $this -> assign( "groupname", $groupname); */
    	$this->display();
            
    }
    public function add_edit(){
        // print_r($_GET);exit;
        $arr = $_GET;
        $id = $_GET['id'];
        $type = $_GET['type'];
        $arr['ctime'] = time();

        unset($arr['id']);
        unset($arr['type']);

        if($type == '1'){//添加
            $re = M('type')->add($arr);
            if($re){
                echojson('添加成功',1);
            }else{
                echojson('添加失败',0);
            }
        }elseif($type == '2'){//修改
            
            $re = M('type')->where("id=$id")->save($arr);
            if($re){
                echojson('修改成功',1);
            }else{
                echojson('修改失败',0);
            }
        }
    }
    public function del(){
        $id = $_GET['id'];

        $re = M('type')->where("id=$id")->save(array('status'=>3));
        if($re){
            echojson('删除成功',1);
        }else{
            echojson('删除失败',0);
        }
    }

    
}