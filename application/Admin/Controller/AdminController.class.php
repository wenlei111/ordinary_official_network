<?php
/*管理员列表*/
namespace Admin\Controller;

use Think\Page;//分页类

class AdminController extends BaseController {

    public function list_(){
        #+----------------------------------
        # * 查询条件
        #+----------------------------------
        $wheresql = ' 1=1 ';

        $username=trim($_GET['username1']);
        if(!empty($username)){
            $wheresql.=" and username like '%$username%' ";
            $this -> assign( "username1", $username);
        }
        $group_id_=trim($_GET['group_id_']);
        if(!empty($group_id_)){
            $wheresql.=" and group_id = $group_id_ ";
            $this -> assign( "group_id_", $group_id_);
        }
    	#+----------------------------------
    	# * 分页
    	#+----------------------------------
        $db = M( "adminuser" );  
        $page_size = 10; //每页显示记录数  
        $record_sum = count( $db -> field('id') -> where( $wheresql ) -> select() );//记录总数  
		$Page = new Page($record_sum, $page_size);  //实例化分页类
        $list = $db-> 
                field( "adminuser.id,adminuser.username,groupname,logintime,adminuser.status,adminuser.des,adminuser.group_id" ) ->  
                join( "admingroup on adminuser.group_id=admingroup.id" ) ->  
                where( $wheresql) ->  
                order( "adminuser.id " ) ->  
                limit($Page->firstRow.",".$Page->listRows) ->  
                select();  
        // echo M()->getLastSql();     
        foreach ($list as $k => $v) {
            if($list[$k]['logintime']){
                $list[$k]['logintime'] =  date('Y-m-d h:i:s',$list[$k]['logintime']); 
            }else{
                $list[$k]['logintime'] =  '无登录记录'; 
            }
        }
        $this -> assign( "list", $list); //输出文章列表  
        $this -> assign( "page", $Page -> show_wen()); //输出分页  
        #+----------------------------------
        # *  获取分组类型
        #+----------------------------------
        $groupname = M()->query("select * from admingroup");
        $this -> assign( "groupname", $groupname); 
    	$this->display();
            
    }
    public function add_edit(){
        $arr = $_GET;
        $id = $_GET['id'];
        $type = $_GET['type'];

        unset($arr['id']);
        unset($arr['type']);

        if($type == '1'){//添加
            $re = M('adminuser')->add($arr);
            if($re){
                echojson('添加成功',1);
            }else{
                echojson('添加失败',0);
            }
        }elseif($type == '2'){//修改
            
            $re = M('adminuser')->where("id=$id")->save($arr);
            if($re){
                echojson('修改成功',1);
            }else{
                echojson('修改失败',0);
            }
        }
    }
    public function del(){
        $id = $_GET['id'];
        $re = M('adminuser')->where("id=$id")->delete();
        if($re){
            echojson('删除成功',1);
        }else{
            echojson('删除失败',0);
        }
    }

    
}