<?php
/*用户资料*/
namespace Admin\Controller;
use Think\Page;//分页类

class DetailController extends BaseController {

    public function list_(){


        #+----------------------------------
        # * 查询
        #+----------------------------------
        
        // $list = M()->query("SELECT * FROM  goods WHERE status = 1 order by ctime");
        // $this -> assign( "list", $list); 
        #+----------------------------------
        # * 查询条件
        #+----------------------------------
        $wheresql = ' 1=1 ';

        $username=trim($_GET['username1']);
        if(!empty($username)){
            $wheresql.=" and username like '%$username%' ";
            $this -> assign( "username1", $username);
        }
        $group_id_=trim($_GET['group_id_']);
        if(!empty($group_id_)){
            $wheresql.=" and group_id = $group_id_ ";
            $this -> assign( "group_id_", $group_id_);
        }
        #+----------------------------------
        # * 分页
        #+----------------------------------
        $db = M( "goods" );  
        $page_size = 14; //每页显示记录数  
        $record_sum = count( $db -> field('id') -> where( $wheresql ) -> select() );//记录总数  
        $Page = new Page($record_sum, $page_size);  //实例化分页类
        $list = $db-> 
                // field( "" ) ->  
                // join( "admingroup on adminuser.group_id=admingroup.id" ) ->  
                where( $wheresql) ->  
                order( "id desc" ) ->  
                limit($Page->firstRow.",".$Page->listRows) -> 
                select();  
        // echo M()->getLastSql();     
        foreach ($list as $k => $v) {
             
            $list[$k]['ctime'] =  date('Y-m-d h:i:s',$list[$k]['ctime']); 
            $pid = M()->query("SELECT name FROM  type WHERE id= $v[pid]");
            $list[$k]['pid'] = $pid[0]['name'];
            $ppid = M()->query("SELECT name FROM  type WHERE id= $v[ppid]");
            $list[$k]['ppid'] = $ppid[0]['name'];
             
        }
        $this -> assign( "list", $list); //输出文章列表  
        $this -> assign( "page", $Page -> show_wen()); //输出分页

        #+----------------------------------
        # * 显示页面
        #+----------------------------------
    	$this->display();
    }

    public function add_edit(){
        #+----------------------------------
        # *  获取二级分类
        #+----------------------------------
        $group = M()->query("select * from type where pid != 0 and status != 3");
        $this -> assign( "group", $group); 
        

        $ids = $_GET['ids'];
        if($ids){//修改
            
            //查询
            $list_xg = M()->query("SELECT * FROM  goods WHERE id = $ids");
            $this -> assign( "list_xg", $list_xg[0]);
            $this -> assign( "ids", $ids);
            // print_r($list_xg);
            $this->display('list_');
            exit;

        }


        if($_GET){
            // $arr = $_GET;
            $pic_fm = strstr($_GET['pic_fm'],'mr.png');
            $pic1 = strstr($_GET['pic1'],'mr.png');
            $pic2 = strstr($_GET['pic2'],'mr.png');
            $pic3 = strstr($_GET['pic3'],'mr.png');

            // echo $pic_fm;exit;
            if($pic_fm){
                unset($_GET['pic_fm']);
            }
            if($pic1){
                unset($_GET['pic1']);
            }
            if($pic2){
                unset($_GET['pic2']);
            }
            if($pic3){
                unset($_GET['pic3']);
            }
            $arr = $_GET;
            $arr['ctime']=time();
            //获取一级分类
            $pid = $_GET['pid'];
            $get_pid = M()->query("SELECT * FROM  type WHERE id = $pid");
            $get_pid = $get_pid[0]['pid'];
            $arr['ppid'] = $get_pid;
            $res = M('goods')->add($arr);

            if($res){
                echojson('成功',1);
            }else{
                echojson('失败',0);
            }
            // print_r($_GET);exit;

        }
        
         
       
        $this->display('list_');
        
        
    }

}