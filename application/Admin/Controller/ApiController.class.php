<?php
namespace Admin\Controller;
use Think\Controller;
use Think\Upload;//导入上传类

//文件上传类
class ApiController extends Controller
{
	public function upload(){//本地单文件上传
        $upload_dir =$_SERVER['CONTEXT_DOCUMENT_ROOT'].__ROOT__.'/public/images/avater/';

        $upload = new \Think\Upload();// 实例化上传类
	    $upload->maxSize   =     1000000*5 ;// 设置附件上传大小1M
	    $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
	    $upload->rootPath  =     $upload_dir; // 设置附件上传根目录
	    // $upload->savePath  =     ''; // 设置附件上传（子）目录
		$upload->autoSub   =     false;       //是否生成子目录
	    // 上传文件 
	    $info   =   $upload->uploadOne($_FILES['file']);

	    if(!$info) {// 上传错误提示错误信息
	        echojson($upload->getError());
	    }else{// 上传成功
	    	$path = __ROOT__.'/public/images/avater/';
	        echojson('上传成功',1,$path.$info['savename']);
	    }
    }//end

}


