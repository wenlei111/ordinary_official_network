<?php
/*类型筛选*/
namespace Admin\Controller;

use Think\Page;//分页类

class TypesxController extends BaseController {

    public function list_(){
        #+----------------------------------
        # * 查询条件
        #+----------------------------------
        $wheresql = ' and 1=1 ';

        $name_=trim($_GET['name_']);
        if(!empty($name_)){
            $wheresql.=" and name like '%$name_%' ";
            $this -> assign( "name_", $name_);
        }
        $type_id=trim($_GET['type_id']);
        if(!empty($type_id)){
            $wheresql.=" and type_id = $type_id ";
            $this -> assign( "type_id", $type_id);
        }
        #+
    	#+----------------------------------
    	# * 
    	#+----------------------------------
        $list = M()->query("select * from typesx where pid = 0 and status != 3 $wheresql");
        
        foreach ($list as $k => $v) {
            $list[$k]['ctime'] =  date('Y-m-d h:i:s',$list[$k]['ctime']); 
            $list[$k]['data'] = M()->query("select * from typesx where pid = $v[id] and status != 3");
            foreach ($list[$k]['data'] as $kk => $vv) {
                $list[$k]['data'][$kk]['ctime'] =date('Y-m-d h:i:s',$list[$k]['data'][$kk]['ctime']);
            }
            $aa =  M()->query("select * from type where id = $v[type_id]");
            $list[$k]['type_id_txt'] = $aa[0]['name'];

        }
        // print_r($list);
        $this -> assign( "list", $list); 
        #+----------------------------------
        # *  获取二级分类
        #+----------------------------------
        $group = M()->query("select * from type where pid != 0 and status != 3");
        $this -> assign( "group", $group); 
        #+----------------------------------
        # *  获取筛选
        #+----------------------------------
        /*$group = M()->query("select * from type where pid != 0 and status != 3");
        $this -> assign( "group", $group); */


    	$this->display();
            
    }
    public function add_edit(){
        // print_r($_GET);exit;
        $arr = $_GET;
        $id = $_GET['id'];
        $type = $_GET['type'];
        $arr['ctime'] = time();

        unset($arr['id']);
        unset($arr['type']);

        if($type == '1'){//添加
            unset($arr['type_id_ture']);
            // print_r($arr);exit;
            $re = M('typesx')->add($arr);
            if($re){
                echojson('添加成功',1);
            }else{
                echojson('添加失败',0);
            }
        }elseif($type == '2'){//修改
            unset($arr['type_id_ture']);
            $re = M('typesx')->where("id=$id")->save($arr);
            if($re){
                echojson('修改成功',1);
            }else{
                echojson('修改失败',0);
            }
        }elseif($type == '3'){//添加二级筛选
            $arr['type_id']=$arr['type_id_ture'];
            unset($arr['type_id_ture']);

            $arr['pid'] = $id;
            // print_r($arr);exit;
            $re = M('typesx')->add($arr);
            if($re){
                echojson('添加成功',1);
            }else{
                echojson('添加失败',0);
            }
        }elseif($type == '4'){//编辑二级筛选
            $arr['type_id']=$arr['type_id_ture'];
            unset($arr['type_id_ture']);
            // print_r($arr);exit;
            $re = M('typesx')->where("id=$id")->save($arr);
            if($re){
                echojson('修改成功',1);
            }else{
                echojson('修改失败',0);
            }
        }
    }
    public function del(){
        $id = $_GET['id'];

        $re = M('typesx')->where("id=$id")->save(array('status'=>3));
        if($re){
            echojson('删除成功',1);
        }else{
            echojson('删除失败',0);
        }
    }

    
}