<?php
/*
 GD库封装
*/
namespace Admin\Controller;
use Think\Controller;
/**
 * GDImg is the model behind the image.
 *
 * //设定图片尺寸
 * $width = 300;
 * $height = 150;
 * //图片一
 * $src = '001.jpg';
 * //$content = 'hello';
 * //$font_url = 'arial.ttf';
 * //$size = 20;
 * //$color = [255,255,255,20];
 * //$local = ['x' => 20, 'y' => 30];
 * //$angle = 10;
 * //图片二
 * $source = "002.jpg";
 * $local = ['x' => 0, 'y' => $height]; //图片位置
 * $alpha = 100; //图片透明度
 *
 * $image = new Image($src);
 * // TODO：以下宽高为配置的主面板显示宽高和图片一宽高
 * $image->thumb($width, $height*2, $width, $height); //压缩图片
 * //$image->fontMark($content, $font_url, $size, $color, $local, $angle); //合成文字水印
 * $image->imageMark($source, $local, $alpha, $width, $height); //合成图片水印并压缩
 * $image->show(); //打印在浏览器
 * //$image->save('abc'); //保存在硬盘中
 */
class GdController extends Controller{
// class Water {
    // 设置最大宽度，用来在编辑器中使用和显示
    private  $max_width  = null;
    private  $file_name  = null;
    private  $water_name = null;
    private  $real       = null; // 获得文件名
    //获得文件名和图片宽度
    public function __construct($max_widht,$file_name,$water_name) {
        $this->max_width  = $max_widht;
        if(strpos($_SERVER['SERVER_NAME'], "sinaapp")){
             $this->file_name = "http://buyingfeiblog-public.stor.sinaapp.com/".$file_name;
        }else{
             $this->file_name  = $file_name;
        }
        $this->water_name = $water_name;
        $this->real      =$file_name;     
    }
    public function create_image(){
        // 获得ori图片信息
        list($width,$height,$type) = getimagesize($this->file_name);   
        // 当原有图片大于 要求的最大宽度时，才需要进行压缩
        if($width > $this->max_width){
            // 获得图片压缩百分比
            $per = $this->max_width / $width;
            $new_width = $width * $per;
            $new_height = $height * $per;          
        }else{
            $new_height =  $height;
            $new_width  =  $width;
        }
//        $this->image_header($type);
        //创建一个真彩色图像
        $image_p = imagecreatetruecolor($new_width, $new_height);
        $image = $this->image_obj($type,  $this->file_name); 
        imagecopyresized($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        $this->image_dump($type, $image_p);
        imagedestroy($image_p);
        $this->water();
    }

    /*
     * 生成为图片添加水印
     */
    private function water(){
        if($this->max_width > 200){
             $water_name = $this->water_name;
            $dist_name = $this->file_name;
            list($dist_width,$dist_height,$type) = getimagesize($dist_name);
            $dist_im   = $this->image_obj($type, $this->file_name);
    //        $water_name = "D:/xampps/htdocs/buyingfeiblog/1/App/Modules/Admin/Tpl/Public/Images/water.png";
            list($w_width,$w_height) = getimagesize($water_name); // 获得图片水印信息

            $water_src =  imagecreatefrompng($water_name);

            // 设置图片水印位置 在右下角
            $x = ($dist_width - $w_width) / 4 * 3 ;
            $y =($dist_height - $w_height) /4 * 3 ;
            if(imagecopy($dist_im, $water_src, $x, $y, 0, 0, $w_width, $w_height)){
                $this->image_dump($type, $dist_im);
            }  
        }

    }
    // 生成图片类型，生成不同图片 保持图片原本类型不发生变化
    private function image_dump($type,$image_p){
        // 如果是在sae 上，需先写入到缓冲区
        if(strpos($_SERVER['SERVER_NAME'], "sinaapp")){
            switch ($type){
                case 1:
                    ob_start();// 开启缓冲
                    imagegif($image_p);
                    $imgstr= ob_get_contents();
                    break;
                case 2:
                    ob_start();// 开启缓冲
                    imagejpeg($image_p);
                    $imgstr= ob_get_contents();
                    break;
                case 3:
                    ob_start();// 开启缓冲
                    imagepng($image_p);
                    $imgstr= ob_get_contents();
                    break;
                default :
            }
            $s = new SaeStorage();
            $s->write('public',$this->real,$imgstr);
            ob_end_clean();
         }else{
            switch ($type){
                case 1:
                    imagegif($image_p,$this->file_name);
                    break;
                case 2:
                    imagejpeg($image_p,$this->file_name);
                    break;
                case 3:
                    imagepng($image_p,$this->file_name);
                    break;
                default :
            }
        }
        imagedestroy($image_p);
    }
    // 根据图片不同，生成不同资源对象
    private function image_obj($type,$filename){
         switch ($type){
//          1 = GIF，2 = JPG，3 = PNG，
            case 1:
               $image =   imagecreatefromgif($filename);
              break;
            case 2:
               $image =   imagecreatefromjpeg($filename);
              break;
            case 3:
               $image =   imagecreatefrompng($filename);
               break;
            default :
        }
        return $image;
    }
}

   /*$thumb = new  Thumb(725,"D:/xampps/htdocs/test/test.jpg");
   $thumb->create_image();//create_image*/