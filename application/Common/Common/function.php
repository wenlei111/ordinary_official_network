<?php
/*输出json格式*/
function echojson($msg = '', $status = 0, $dataarr = array()) {
    $arr = array (
        'msg'    => $msg,
        'status' => $status.''
    );
    if(!empty($dataarr)){
        $arr['data'] = $dataarr;
    }
    echo json_encode($arr);
    exit;
}
/*检查后台登录权限*/
function check_qx_admin($concoller,$action){
	//获取uid
	$uid = $_COOKIE['wen_admin_userid'];
	//获取用户权限组 group_id
	$group_id = $_SESSION["admin_userinfo".$uid]['group_id'];
	// echo $group_id;

	//查group_id对应的权限
	$rule_str = M()->query("select rule_str from admingroup where id = $group_id");
	$rule_arr = explode(',',$rule_str[0]['rule_str']);
	// print_r($rule_arr);

	//获取控制器和方法对应的adminrule表id
	$re = M('adminrule')->where("ac = '$concoller' and op = '$action'")->find();
	$adminrule_id = $re['id'];
	// print_r($re['id']);

	if( in_array($adminrule_id,$rule_arr) ){
		return ture;//有权限

	}else{
		return false;//无权限
	}

	
}
/*
货币基金复利计算
$day 天数
$money 初始资金
$w_sy  万份收益
*/
function fund_fl($day,$money,$w_sy){
	
	//一块钱一天多少
	// $money = 5000;
	//10000块1年多少复利
	for ($i=0; $i < $day; $i++) { //10天
		$t = $i+1;
		if($i != 0){
			$money = $money + ($w_sy/10000)*$money;
			// echo 'day'.$t.'='.$money.'+'.($w_sy/10000)*$money.'='.($money+$w_sy/10000*$money).'<br>';
		}else{
			// echo 'day'.$t.'='.$money.'<br>';
		}
	}
	// $one_dey = (1.50/10000);
	echo $money;
}

/*
图片添加文字水印
$bg_img   背景图路径
$text      水印文字
*/
function water_str($filename,$text){
	//获取图片信息
	$fileinfo = getimagesize($filename);
	$width = $fileinfo[0];
	$height = $fileinfo[1];
	$type = $fileinfo[2];//1 gif,2 jpeg,3 png	
	$type_array = array(1=>"gif",2=>"jpeg",3=>"png");
	//获取图片格式的字符串形式
	$type_str = $type_array[$type];
	//从现有图片上获取资源
	//拼接函数名称
	$fun = "imagecreatefrom".$type_str;
	$img = $fun($filename);
	//字体
	$font = $_SERVER['CONTEXT_DOCUMENT_ROOT'].__ROOT__."/public/font/msyh.ttf";
	//颜色
	$color = imagecolorallocate($img,255,0,0);
	$size = 40;
	$angle = 0;
	imagefttext(
			$img,
			$size,  //字体大小
			$angle, //角度
			$x,$y,  //水印文字位置
			$color, //颜色
			$font, 
			$text
		);
	//输出图片
	header("content-type:image/png");
	imagepng($img);
}

/*
图片添加图片水印
$filename   背景图路径
$water      水印图路径
*/

function water_pic($filename,$water){
	#+------------------------
	# * 获取底图资源
	#+------------------------
	$fileinfo = getimagesize($filename);
	$width = $fileinfo[0];//图片的宽
	$height = $fileinfo[1];//图片的高
	$type = $fileinfo[2];//图片格式 数值类型
	$type_array = array(1=>"gif",2=>"jpeg",3=>"png");
	//获取大图片的图片格式
	$type_str = $type_array[$type];
	//拼接函数
	$fun_str = "imagecreatefrom";
	//获取大图片资源的函数名称
	$des_fun = $fun_str.$type_str;
	//$des_fun = imagecreatefromgif	
	//目标图片/大图片的资源
	$des_img = $des_fun($filename);
	#+---------------------end
	
	#+------------------------
	# * 获取小图片资源
	#+------------------------
	$w_fileinfo = getimagesize($water);
	$w_width = $w_fileinfo[0];
	$w_height = $w_fileinfo[1];
	$w_type = $w_fileinfo[2];//小图片的图片格式 数值类型
	//获取小图片的图片格式 字符类型
	$w_type_str = $type_array[$w_type];
	//拼接函数（小图片资源）
	$src_fun = $fun_str.$w_type_str;	
	$src_img = $src_fun($water);
	/*$des_x = $width - $w_width - 20;
	$des_y = $height - $w_height - 20; */
	#+---------------------end

	#+------------------------
	# * 压缩水印图片
	#+------------------------
	//修改水印图片大小  压缩
	$src_img_ys= imageCreatetruecolor (200, 200);//创建头像画布
	$color = imagecolorallocate(
						$src_img_ys, 
						255, 255, 255,//白色背景
						0//透明
					);
	imageColorTransparent($src_img_ys, $color);//合成
	imagecopyresampled ( 
		$src_img_ys , //新建压缩图背景
		$src_img ,    //需要压缩图片
		0 , 0 , //设定需要载入的图片在新图中的坐标
		0 , 0 , //设定载入图片要载入的区域
		100 ,   //设定载入的原图的宽度（在此设置缩放）
		100 ,	//设定载入的原图的高度（在此设置缩放）
		$w_width , $w_height //原图要载入的宽、高度
		);
	#+---------------------end

	//合成图像
	imagecopymerge($des_img,$src_img_ys,
	          100,   //目标图像开始 y 坐标
	          100,   //目标图像开始 y 坐标，从左上角开始
	          0,0,  // 拷贝图像开始 x 坐标
	          $w_width,$w_height, //拷贝的宽、高度
	          100  //图片合成度0-100  透明度
	          );
	
	header("content-type:image/png");
	imagepng($des_img);
	
	imagedestroy($des_img);
	imagedestroy($src_img);
}

/*
打印日志
*/
function dolog($msg='',$datas=''){
    $time = date('H:i:s',time());
    $filename = $_SERVER['CONTEXT_DOCUMENT_ROOT'].__ROOT__."/application/Runtime/Logs/".date('Y-m-d',time()).'my.log';
    if(is_array($datas)){
        file_put_contents($filename, "[{$time}]\n"."{$msg}\n".var_export($datas,true)."\n\n", FILE_APPEND);
    }else{
        file_put_contents($filename, "[$time]\n"."{$msg}\n".$datas."\n\n", FILE_APPEND);
    }
}
/*
打印接口:（跳转链接）
*/
function apilog($uid) {
    if (empty($content)) {
        $content = date("Y-m-d H:i:s") . '--' . $uid. ':' . "\r\n" .'https://'.$_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] . "\r\n\n";
    }
    $filename = $_SERVER['CONTEXT_DOCUMENT_ROOT'].__ROOT__."/application/Runtime/Logs/api.log";
    file_put_contents($filename, $content, FILE_APPEND);

}