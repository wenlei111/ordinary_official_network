<?php
namespace Home\Controller;
use Think\Controller;

//自定义的，所有控制器的父类
class BaseController extends Controller
{
	//所有继承这个父类的方法都要自动检测跳转权限
	public function _initialize(){
        // print_r($_SERVER);
        if(!check_qx_admin(CONTROLLER_NAME,ACTION_NAME)){
            $this->display('Result:a403');
           die;
        };
        // apilog();
	}
}


