<?php
header("content-type:text/html;charset=utf-8");

//指定绑定的模块(默认：Home)
define("BIND_MODULE","Home");
//指定应用程序的目录
define("APP_PATH","application/");
//开启调试功能(默认：false)
define("APP_DEBUG",true);
//构建目录安全(默认：true)
define("BUILD_DIR_SECURE",true);

include_once 'library/ThinkPHP/ThinkPHP.php';