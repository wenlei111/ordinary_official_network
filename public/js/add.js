
//显示请求结果
function returnInfo(res){
    try{
        res=JSON.parse(res);
        // alert(res.msg);
    }catch(err){
        layer.msg("返回数据有问题", {icon: 2, time: 1200  });

    }
    
    switch (parseInt(res.status)) {
        case 1:
            layer.msg(res.msg, {
                icon: 1,
                time: 1000
            });
            setTimeout(function() {
                if (res.data == null || res.data == '') {
                    window.location.reload();
                } else {
                    window.location.href = res.data;
                }

            }, 1300);
            break;
        case 0 :
            layer.msg(res.msg, {
                icon: 2,
                time: 2200
            });
            break;
        default:
            layer.msg("返回数据有问题", {
                icon: 2,
                time: 1200
            });
            break;
    }
}
