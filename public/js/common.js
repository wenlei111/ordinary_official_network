
//显示请求结果
function returnInfo(res){
    try{
        res=JSON.parse(res);
        // alert(res.msg);
    }catch(err){
        layer.msg("返回数据有问题", {icon: 2, time: 1200  });

    }
    
    switch (parseInt(res.status)) {
        case 1:
            layer.msg(res.msg, {
                icon: 1,
                time: 1000
            });
            setTimeout(function() {
                if (res.data == null || res.data == '') {
                    window.location.reload();
                } else {
                    window.location.href = res.data;
                }

            }, 1300);
            break;
        case 0 :
            layer.msg(res.msg, {
                icon: 2,
                time: 2200
            });
            break;
        default:
            layer.msg("返回数据有问题", {
                icon: 2,
                time: 1200
            });
            break;
    }
}
//reset清空input
function resetAll() {
    $("#search").find('input[type=text],select,input[type=hidden]').each(function() {
        $(this).val('');
    });
}

function close_(){
  // alert(11);
  layer.closeAll();
}


// ifram加载
function jump_page(url,class_,remark){
    // alert(remark);
    $("dd[title="+remark+"]").attr('class','layui-this');
    // $("dd[title="+remark+"]").parent().attr('class','layui-nav-itemed');
    // $("dd[title="+remark+"]").parent().siblings().css('background','red');

    // $("dd[title="+remark+"]").click();
    $("iframe").attr('src','');
    $("#ifr").attr('src',url);
    if(remark){
    //点击加载面包屑导航
    $(".wen_tab").children().remove();
    $(".wen_tab").append('<li><a onclick="jump_page("__APP__/index/main.html")" class="wen_cc" href="">首页</a></li><li class="wen_cc">/</li><li ><a class="wen_cc">'+class_+'</a></li><li class="wen_cc">/</li><li><a class="wen_cc">'+remark+'</a></li>');
    }
}