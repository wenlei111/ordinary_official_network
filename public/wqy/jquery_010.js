(function($) { 
   $.fn.formUtils = function(settings) {
     var config = {
		charLimitAttr: "maxlength",
        charCountDisplaySelector: ".remaining"
     };
     
     if (settings) { $.extend(config, settings); }
 
     this.each(function() {
        var form = this;

        function init() {
            //character count
            var charLimitedInputs = $("input[" + config.charLimitAttr + "], textarea[" + config.charLimitAttr + "]", form);
            charLimitedInputs.on("keyup.formUtils", $.proxy(onkeyup_charcountinput, form));
            charLimitedInputs.trigger("keyup.formUtils");

            //textarea pre-filled value
            fillTextArea();
            //select pre-filled value
            fillSelect();
        };
        //character count
        function validateCharacterCount($input, $form) {
            function lineBreakCount(str) {
                try {
                    return ((str.match(/[^\n]*\n[^\n]*/gi).length));
                } catch (e) {
                    return 0;
                }
            }
            var characters = $input.val(),
                charactersLength = characters.length,
                maxLength = parseInt($input.attr(config.charLimitAttr)),
                charactersRemaining = Math.max(maxLength - charactersLength, 0),
                lineBreaks = lineBreakCount(characters);
            $("p[data-name=\"" + $input.attr("name") + "\"] " + config.charCountDisplaySelector, $form).text(charactersRemaining);
            if (charactersRemaining < 1) {
                $input.val(characters.substr(0, maxLength));
                return false;
            }
            if (lineBreaks > 3) {
                var end = characters.length - lineBreaks;
                // $input.val(characters.substr(0, end));
                // return false;
            }
        };
        function onkeyup_charcountinput(e) {
            //count characters and update characters remaining
            validateCharacterCount($(e.currentTarget), $(form));
        };

        //textarea pre-filled value
        function fillTextArea() {
            $(".textarea-value", form).each(function(index, element){
                var textarea = $("textarea[name=" + $(element).attr("data-target") + "]");
                if (textarea) {
                    textarea.val($(element).html());
                }
            });
        };
        //select pre-filled value
        function fillSelect() {
            $("select[data-selected-option]", form).each(function(index, element) {
                var selectbox = $(element);
                selectbox
                    .val(selectbox.attr("data-selected-option"))
                    .trigger("change")
                    .removeClass("empty");
            });
        };

        init();
     });
 
     return this;
   };
 
 })(jQuery);