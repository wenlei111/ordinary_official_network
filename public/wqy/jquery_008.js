/*
<div class="checkbox-field">
    <input type="checkbox" class="visuallyhidden" value="true" name="rememberMe">
    <div class="checkbox-div"><span class="icon"></span></div>
    <label for="rememberMe">Remember Me</label>
</div>

<div class="checkbox-field">
    <input type="checkbox" value="true" name="rememberMe">
    <label for="rememberMe">Remember Me</label>
</div>
*/

(function($) { 
   $.fn.formStyle = function(settings) {
     var config = {
    		styleSelectBoxes: false,
            radioBtnStyle: "default",
            radioGroup: ".radio-group",
            radioBtnWrapper: ".radiobtn-field",
            radioBtnInput: ".radio-input",
            radioBtnDisplay: ".radio-div",
            checkboxStyle: "default",
            checkboxWrapper: ".checkbox-field",
            checkboxInput: ".checkbox-input",
            checkboxDisplay: ".checkbox-div",
            activeClass: "active"
     };
     
     if (settings) { $.extend(config, settings); }
        
     this.each(function() {
        var form = this;

        function init() {
            //Radio buttons
            var activeRadioButtons = $(config.radioBtnInput, form).filter(":checked");
            if (activeRadioButtons.length > 0) {
                activeRadioButtons.closest(config.radioBtnWrapper)
                    .find(config.radioBtnDisplay)
                    .addClass(config.activeClass);
            }
            $(config.radioBtnDisplay, form)
                .off("click.formStyle") //prevent double binding
                .on("click.formStyle", onclick_radioButton);
            
            //checkbox input
            var activeCheckboxes = $(config.checkboxInput, form).filter(":checked");
            if (activeCheckboxes.length > 0) {
                activeCheckboxes.closest(config.checkboxWrapper)
                    .find(config.checkboxDisplay)
                    .addClass(config.activeClass);
            }
            $(config.checkboxDisplay, form)
                .off("click.formStyle") //prevent double binding
                .on("click.formStyle", onclick_checkbox);
            $(config.checkboxWrapper + " label", form)
                .on("click.formStyle", onclick_checkboxlabel);
      
            //selectboxes
            $("select", form)
                .each(function(index, element){
                    updateSelectState($(element));
                })
                .off("change.formStyle") //prevent double binding
                .on("change.formStyle", onchange_selectbox);
        }	
        //Radio button
        function onclick_radioButton(e) {
            var target = $(e.delegateTarget),
                targetInput = target.closest(config.radioBtnWrapper).find(config.radioBtnInput),
                groupInputs = $(config.radioBtnInput).filter("[name=\"" + targetInput.attr("name") + "\"]").not(targetInput);
            groupInputs.each(function(index, element) {
                $(element)
                    .removeAttr("checked")
                    .closest(config.radioBtnWrapper)
                    .find(config.radioBtnDisplay)
                    .removeClass(config.activeClass).trigger("radioOff", {incoming: targetInput});
            });
            target.addClass(config.activeClass);
            targetInput.attr("checked", "checked").prop("checked", "checked").change().trigger("radioOn");
        };
        //checkbox
        function onclick_checkbox(e) {
            selectCheckbox(e);
        };
        function onclick_checkboxlabel(e) {
            e.preventDefault();
            selectCheckbox(e);
        };
        function selectCheckbox(e) {
            var checkboxWrapper = $(e.delegateTarget).closest(config.checkboxWrapper),
                target = checkboxWrapper.find(config.checkboxDisplay),
                targetInput = checkboxWrapper.find(config.checkboxInput);
            if( (e.isTrigger) || !target.closest(config.radioGroup).hasClass("locked") && !targetInput.is(":disabled")){
                if (targetInput.prop("checked")) {
                    target.removeClass(config.activeClass);
                    targetInput.prop("checked", false).change().trigger("checkOff", { isTrigger: e.isTrigger });
                } else {
                    target.addClass(config.activeClass);
                    targetInput.prop("checked", true).change().trigger("checkOn", { isTrigger : e.isTrigger });
                }
            }
        };
        //selectbox
        function onchange_selectbox(e) {
            var selectbox = $(e.delegateTarget)
            updateSelectState(selectbox);
        };
        function updateSelectState(selectbox) {
            if( selectbox.val() == null || selectbox.val() == "") {
                selectbox.addClass("empty");
            } else {
                selectbox.removeClass("empty");
            }
        };

        init();
        
     });
 
     return this;
   };
 
 })(jQuery);