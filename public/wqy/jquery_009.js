/* jQuery Tiny Pub/Sub - v0.7 - 10/27/2011
 * http://benalman.com/
 * Copyright (c) 2011 "Cowboy" Ben Alman; Licensed MIT, GPL */

(function($) {

  var o = $({});
  var t = {
    subscribers: 0,
    queue: []
  }

  $.subscribe = function(eventName, callback) {
    var topic = o[eventName] || t;

    o.on.apply(o, arguments);

    topic.subscribers++;
    // if first subscriber and there are queued up events
    if(topic.subscribers === 1 && topic.queue.length){
      $.each(topic.queue, function(i, val){
        $.publish(eventName, val);
      });
      topic.queue = [];
    }
    o[eventName] = topic;
  };

  $.unsubscribe = function(eventName) {
    var topic = o[eventName] || t;
    if(topic.subscribers > 0) topic.subscribers--;
    o.off.apply(o, arguments);
    o[eventName] = topic;
  };

  $.publish = function(eventName, data, options) {
    // only publish if someone is listening
    var topic = o[eventName] || t;
    options = options || {};
    if(topic.subscribers || !options.queueMessage){
      o.trigger.apply(o, arguments);
    } else {
      topic.queue.push(data);
    }
    o[eventName] = topic;
  };

}(jQuery));