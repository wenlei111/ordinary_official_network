(function($) { 
   $.fn.touchswipe = function(settings) {
     var config = {
    		xThreshold: 20,
    		yThreshold: 20,
 			scrollX: function() { },
 			scrollY: function() { },
 			swipeLeft: function() { },
 			swipeRight: function() { },
 			swipeUp: function() { },
 			swipeDown: function() { },
			preventDefaultEvents: true,
			preventDefaultThreshold: 10
	 };
     
     if (settings) $.extend(config, settings);
 
     this.each(function() {
    	 var startX;
    	 var startY;
		 var isMoving = false;

    	 function cancelTouch() {
    		 this.removeEventListener("touchmove", onTouchMove);
    		 startX = null;
    		 isMoving = false;
    	 }	
    	 
    	 function onTouchMove(e) {
    		 if (config.preventDefaultEvents) {
    			 e.preventDefault();
    		 }
    		 if (isMoving) {
	    		 var x = e.touches[0].pageX;
	    		 var y = e.touches[0].pageY;
	    		 var dx = startX - x;
	    		 var dy = startY - y;
	    		 if (dx > config.preventDefaultThreshold) {
	    			 e.preventDefault();
	    		 }
	    		 config.scrollX(dx);
	    		 config.scrollY(dy);
	    		 if (Math.abs(dx) >= config.xThreshold) {
	    			cancelTouch();
	    			if (dx > 0) {
	    				config.swipeLeft();
	    			}
	    			else {
	    				config.swipeRight();
	    			}
	    		 }
	    		 else if (Math.abs(dy) >= config.yThreshold) {
	    			cancelTouch();
	    			if(dy > 0) {
	    				config.swipeDown();
	    			}
	    			else {
	    				config.swipeUp();
	    			}
	    		 }
    		 }
    	 }
    	 
    	 function onTouchStart(e) {
    		 if (e.touches.length == 1) {
    			 startX = e.touches[0].pageX;
    			 startY = e.touches[0].pageY;
    			 isMoving = true;
    			 this.addEventListener("touchmove", onTouchMove, false);
    		 }
    	 }
    	 
    	 if ("ontouchstart" in document.documentElement) {
    		 this.addEventListener("touchstart", onTouchStart, false);
    	 }
    	 
     });
 
     return this;
   };
 
 })(jQuery);